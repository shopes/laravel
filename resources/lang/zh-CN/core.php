<?php
/**
 * Date: 2017/1/6
 */

return [
    
    'user_not_exists' => '用户不存在',
    
    'email_has_registed' => '邮箱 :email 已经注册过！',

];
