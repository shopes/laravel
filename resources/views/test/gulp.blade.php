<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
    {{-- Vue入口 --}}
    <div id='entry'></div>
</body>

{{-- CDN --}}
<script src="//cdn.bootcss.com/vue/1.0.26/vue.min.js"></script>

{{-- 引进编译后的js文件 --}}
<script src="{{ asset('/assets/js/hello.js') }}" charset="utf-8"></script>
</html>