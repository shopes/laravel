<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package howlthemes
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <base href="{{ get_bloginfo('home') }}/"/>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <!--Basic Styles-->

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->
    {{--<link href="http://fonts.useso.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">--}}

    <!--Beyond styles-->
    <link id="beyond-link" href="assets/css/beyond.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/demo.min.css" rel="stylesheet" />
    <link href="assets/css/typicons.min.css" rel="stylesheet" />
    <link href="assets/css/animate.min.css" rel="stylesheet" />
    <link id="skin-link" href="" rel="stylesheet" type="text/css" />

    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
    <script src="assets/js/skins.min.js"></script>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope="itemscope" itemtype="http://schema.org/WebPage">

@include("wp::inc.header-common")

<div id="page" class="hfeed site">

    <header id="masthead" class="site-header" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
        <div class="header-inner">
            <div class="site-branding">
                <?php
                $logo_image = '';
                if(function_exists('get_custom_logo')) {
                    $logo_image = has_custom_logo();
                    $output_logo = get_custom_logo();
                }
                if(empty($logo_image)){?>
                <?php if (is_single() || is_page()) { ?>
                <h2 class="site-title" itemprop="headline"><a href="<?php echo esc_url(home_url('/')); ?>"
                                                              rel="home"><?php bloginfo('name'); ?></a></h2>
                <?php } else{?>
                <h1 class="site-title" itemprop="headline"><a href="<?php echo esc_url(home_url('/')); ?>"
                                                              rel="home"><?php bloginfo('name'); ?></a></h1>
                <?php } ?>
                <h2 class="site-description" itemprop="description"><?php bloginfo('description'); ?></h2>
                <?php }
                else {
                    echo $output_logo;
                }?>
            </div><!-- .site-branding -->

            <div id="respo-navigation">
                <nav id="site-navigation" class="main-navigation" itemscope="itemscope"
                     itemtype="http://schema.org/SiteNavigationElement">
                    <?php wp_nav_menu(array('theme_location' => 'primary', 'menu_id' => 'primary-menu')); ?>
                </nav><!-- #site-navigation -->

            </div>
            <div id="mobile-header">
                <a id="responsive-menu-button" href="#sidr-main"><i class="fa fa-bars"></i></a>
            </div>
        </div>
    </header><!-- #masthead -->


    <div id="content" class="site-content">
