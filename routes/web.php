<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('upload/{filename}', function ($filename) {
    $path = storage_path() . '/' . $filename;

    if (!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('images/{filename}', function ($filename) {
    return Image::make(storage_path() . '/' . $filename)->response();
});

Route::get("gulp", function () {
    App::stop(true);
    return view('test.gulp');
});
Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get("/phpinfo", function () {
    phpinfo();
    exit;
});


