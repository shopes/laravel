<?php

//use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


use EasyWeChat\Foundation\Application;

use Upgate\LaravelJsonRpc\Contract\RouteRegistryInterface;
use Upgate\LaravelJsonRpc\Contract\ServerInterface as JsonRpcServerContract;

Route::get('/user', function(Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::any("/weixin", function(Application $wechat, Request $request) {
    $wechat->server->serve()->send();
});


/** @var JsonRpcServerContract $jsonRpcServer */
Route::post("/rpc2", function(Illuminate\Http\Request $request) use ($jsonRpcServer) {
    $jsonRpcServer->router()
        ->addMiddleware(['fooMiddleware', 'barMiddleware'])// middleware alias names or class names
        ->bindController('foo', 'FooController')// for 'foo.$method' methods invoke FooController->$method(),
        // for 'foo' method invoke FooConroller->index()
        ->bind('bar', 'MyController@bar')// for 'bar' method invoke MyController->bar()
        ->group(
            ['bazMiddleware'], // add bazMiddleware for methods in this group
            function(RouteRegistryInterface $jsonRpcRouter) {
                // for 'bar.baz' method invoke MyController->bazz()
                $jsonRpcRouter->bind('bar.baz', 'MyController@bazz');
            }
        );
    
    // Run json-rpc server with $request passed to middlewares as a handle() method argument
    return $jsonRpcServer->run($request);
});


Route::match(['get', 'post'], '/rpc/{ctl}/{method?}/{param?}',
    function(Request $request, $ctl, $method = 'index', $param = null) use ($jsonRpcServer) {
        $origin = $ctl;
        $stack = Route::getGroupStack();
        $namespace = end($stack)['namespace'];
        if(!class_exists($ctl)) {
            $ctl = $namespace . '\\' . $ctl;
        }
        //krumo(["$uniqid.$method" => Request::all()]);
        //$controller = WechatController::class;
        
        if(class_exists($ctl) && method_exists($ctl, Request::getMethod() . $method)) {
            $action = strtolower(Request::getMethod()) . ucfirst($method);
            $jsonRpcServer->router()->bind($origin . '.' . $method, $ctl . '@' . $action);
            return $jsonRpcServer->run($request);
        } else {
            trigger_error('method not found');
        }
    });
