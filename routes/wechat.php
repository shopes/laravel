<?php
/**
 * @since 2017/1/8
 */

//Route::controller('/', 'WechatController');

use Wechat\Http\Controllers\WechatController;

Route::match(['get', 'post'], '/{method}/{uniqid?}',
    function($method, $uniqid = null) {
        krumo(["$uniqid.$method" => Request::all()]);
        $controller = WechatController::class;
        if(method_exists($controller, Request::getMethod() . $method)) {
            $method = strtolower(Request::getMethod()) . ucfirst($method);
            return App::call($controller . '@' . $method, ['uniqid' => $uniqid]);
        } else {
            trigger_error('method not found');
        }
    });

