<?php

namespace App\Console\Commands;

use App;
use base_database_manager;
use File;
use Illuminate\Console\Command;

class UpdateCommand extends Command
{
    /** @var  \Doctrine\DBAL\Connection */
    protected $db;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update {act?}';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        app()->command = $this;
        
        //$this->db = App::make(base_database_manager::class)->connection();
        //kd($this->db->exec('show tables'));
        //
        foreach(\Modules::all() as $module) {
            if(is_dir($path = $module->getPath('Database/Schema'))) {
                /** @var \Symfony\Component\Finder\SplFileInfo $file */
                //                foreach(File::allfiles($path) as $file){
                //                    $this->info($file->getFilename());
                //                }
                $appId = $module->getLowerName();
                (new \base_application_dbtable($appId))->update($appId);
            }
            
        }
    }
}
