<?php

namespace App\Console\Commands;

use App;
use Cache;
use Config;
use Exception;
use Illuminate\Console\Command;
use Lang;
use Modules;
use Setting;



class DevCommand extends Command
{
    public $private_key;
    public $public_key;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dev {app?} {act?}';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    
    public function __construct($input = null, $output = null)
    {
        parent::__construct();
        if(isset($input)) {
            $this->input = $input;
        }
        
        if(isset($output)) {
            $this->output = $output;
        }
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $args = $this->Arguments();
        //kd($this->Arguments());
        $app = $args['app'] ?: 'index';

        if(method_exists($this, 'cmd_' . $app)) {
            return $this->{"cmd_$app"}($args['act']);
        } elseif(class_exists($className = __NAMESPACE__ . '\Dev\\' . ucfirst($app))
        || class_exists($className = __NAMESPACE__ . '\Dev\\' . ucfirst($app)."Command")
        ) {
            if(method_exists($className, $method_name = 'cmd_' . $args['act'])
                || method_exists($className, $method_name = 'cmd_' . $app)
                || method_exists($className, $method_name = 'cmd')
            ) {
                $cmd = new $className($this->input, $this->output);
                return call_user_func_array([$cmd, $method_name], [$args['act']]);
                //return App::call([$cmd, $method_name], isset($args['act']) ? [$args['act']] : []);
            }
        }
        $this->info("command dev {$app} not exists. ");
    }
    
    

    
    public function cmd_config()
    {
        kd(\Config::get('module.admin.name'));
    }
    

    
    public function cmd_lang()
    {
        kd(trans('core.user_not_exists'));
    }
     

    

    
}
