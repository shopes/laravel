<?php
/**
 * @since 2017/1/8
 */

namespace App\Console\Commands\Dev;


use App\Console\Commands\DevCommand;
use Setting;

class SecacheCommand extends DevCommand
{
    public function cmd()
    {
        Setting::forever('test', ['id' => 123]);
        kd(Setting::get('test'));
    }
    
    public function cmd_check(){
        echo strlen('$Rev: 34670 $');
    }
}