<?php
/**
 * @since 2017/1/8
 */

namespace App\Console\Commands\Dev;


use App\Console\Commands\DevCommand;
use Config;

use Exception;
use Mdanter\Ecc\EccFactory;
use Mdanter\Ecc\Crypto\Signature\Signer;
use Mdanter\Ecc\Math\GmpMathInterface;
use Mdanter\Ecc\Serializer\Signature\DerSignatureSerializer;
use Mdanter\Ecc\Serializer\PublicKey\DerPublicKeySerializer;
use Mdanter\Ecc\Serializer\PublicKey\PemPublicKeySerializer;
use Mdanter\Ecc\Serializer\PrivateKey\PemPrivateKeySerializer;
use Mdanter\Ecc\Serializer\PrivateKey\DerPrivateKeySerializer;

class EccCommand extends DevCommand
{
    
    public function cmd_ecc($act = null)
    {
        if($act == 'key') {
            $adapter = EccFactory::getAdapter();
            $generator = EccFactory::getNistCurves()->generator256();
            $private = $generator->createPrivateKey();
            
            $keySerializer = new PemPrivateKeySerializer(new DerPrivateKeySerializer($adapter));
            $data = $keySerializer->serialize($private);
            echo $data . PHP_EOL;
            $pubKey = $private->getPublicKey();
            $keySerializer = new PemPublicKeySerializer(new DerPublicKeySerializer($adapter));
            
            $data = $keySerializer->serialize($pubKey);
            echo $data . PHP_EOL;
            
        } elseif($act == 'sign') {
            $this->private_key = Config::get("local.keys.user.private");
            $data = serialize(['test' => 123]);
            $sign = $this->sign($data, $this->private_key);
            k($sign);
            $this->public_key = Config::get("local.keys.user.public");
            //kd($this->public_key);
            $veridate = $this->verisign($sign, $data, $this->public_key);
            k($sign, $veridate);
        } elseif($act == 'ecdsa') {
            $this->ECDSA(Config::get("local.keys.app.private"), Config::get("local.keys.user.public"), $shared);
            $shared = gmp_strval($shared, 36);
            echo "Shared secret: " . $shared . PHP_EOL;
            $this->ECDSA(Config::get("local.keys.user.private"), Config::get("local.keys.app.public"), $shared);
            
            // test
        }
    }
    /**
     * 用于url的base64encode
     * '+' => '*', '/' => '-', '=' => '_'
     * @param string $string 需要编码的数据
     * @return string 编码后的base64串，失败返回false
     */
    private function base64Encode($string)
    {
        static $replace = Array('+' => '*', '/' => '-', '=' => '_');
        $base64 = base64_encode($string);
        if($base64 === false) {
            throw new Exception('base64_encode error');
        }
        return str_replace(array_keys($replace), array_values($replace), trim($base64, '='));
    }
    
    private function base64Decode($string)
    {
        static $replace = Array('+' => '*', '/' => '-', '=' => '_');
        $base64 = base64_decode(str_replace(array_values($replace), array_keys($replace), $string));
        if($base64 === false) {
            throw new Exception('base64_decode error');
        }
        return $base64;
    }
    
    
    /**
     * ECDSA-SHA256签名
     * @param string $data 需要签名的数据
     * @param $privateKey
     * @return string 返回签名 失败时返回false
     */
    private function sign($document, $privateKey)
    {
        
        $adapter = EccFactory::getAdapter();
        $generator = EccFactory::getNistCurves()->generator256();
        $useDerandomizedSignatures = true;
        $algorithm = 'sha256';
        
        ## You'll be restoring from a key, as opposed to generating one.
        $pemSerializer = new PemPrivateKeySerializer(new DerPrivateKeySerializer($adapter));
        $key = $pemSerializer->parse($privateKey);
        
        
        $signer = new Signer($adapter);
        $hash = $signer->hashData($generator, $algorithm, $document);
        
        # Derandomized signatures are not necessary, but can reduce
        # the attack surface for a private key that is to be used often.
        if($useDerandomizedSignatures) {
            $random = \Mdanter\Ecc\Random\RandomGeneratorFactory::getHmacRandomGenerator($key, $hash, $algorithm);
        } else {
            $random = \Mdanter\Ecc\Random\RandomGeneratorFactory::getRandomGenerator();
        }
        
        $randomK = $random->generate($generator->getOrder());
        $signature = $signer->sign($key, $hash, $randomK);
        
        $serializer = new DerSignatureSerializer();
        $serializedSig = $serializer->serialize($signature);
        return base64_encode($serializedSig);
    }
    
    public function verisign($sign, $document, $publicKey)
    {
        $adapter = EccFactory::getAdapter();
        $generator = EccFactory::getNistCurves()->generator384();
        $algorithm = 'sha256';
        $sigData = base64_decode($sign);
        
        // Parse signature
        $sigSerializer = new DerSignatureSerializer();
        $sig = $sigSerializer->parse($sigData);
        
        // Parse public key
        $derSerializer = new DerPublicKeySerializer($adapter);
        $pemSerializer = new PemPublicKeySerializer($derSerializer);
        $key = $pemSerializer->parse($publicKey);
        
        $signer = new Signer($adapter);
        $hash = $signer->hashData($generator, $algorithm, $document);
        $check = $signer->verify($key, $sig, $hash);
        return $check;
    }
    
    public function ECDSA($privateKey, $publicKey, &$shared)
    {
        // ECDSA domain is defined by curve/generator/hash algorithm,
        // which a verifier must be aware of.
        
        $adapter = EccFactory::getAdapter();
        $generator = EccFactory::getNistCurves()->generator384();
        $useDerandomizedSignatures = true;
        
        
        $pemPriv = new PemPrivateKeySerializer(new DerPrivateKeySerializer());
        $pemPub = new PemPublicKeySerializer(new DerPublicKeySerializer());
        
        # These .pem and .key are for different keys
        $alicePriv = $pemPriv->parse($privateKey);
        $bobPub = $pemPub->parse($publicKey);
        if(empty($shared)) {
            $exchange = $alicePriv->createExchange($bobPub);
            $shared = $exchange->calculateSharedKey();
            
        } else {
            //echo "Shared secret: " . $shared . PHP_EOL;
            
            $shared = gmp_init($shared, 36);
        }
        
        
        # The shared key is never used directly, but used with a key derivation function (KDF)
        $kdf = function(GmpMathInterface $math, \GMP $sharedSecret) {
            $binary = $math->intToString($sharedSecret);
            $hash = hash('sha256', $binary, true);
            return $hash;
        };
        
        $key = $kdf($adapter, $shared);
        echo "Encryption key: " . unpack("H*", $kdf($adapter, $shared))[1] . PHP_EOL;
        
        # This key can now be used to encrypt/decrypt messages with the other person
    }
    
    
}