<?php
/**
 * @since 2017/1/8
 */

namespace App\Console\Commands\Dev;


use App\Console\Commands\DevCommand;

class HashCommand extends DevCommand
{
    public function cmd_hash()
    {
        exec('dmidecode --type=1', $output);
        $output = implode("\n\n", $output);
        
        
        $password = sha1($output);
        $iterations = 52123;
        
        //$size = mcrypt_get_iv_size(MCRYPT_CAST_256, MCRYPT_MODE_CFB);
        //$iv = mcrypt_create_iv($size, MCRYPT_DEV_RANDOM);
        
        // 使用 mcrypt_create_iv() 生成随机初始向量，
        // 也可以使用 openssl_random_pseudo_bytes() 或其他适合的随机源。
        $salt = @mcrypt_create_iv(16, MCRYPT_DEV_URANDOM);
        
        $hash = hash_pbkdf2("sha256", $password, 'moshihui@gmail.com', $iterations, 32);
        kd(get_defined_vars());
    }
}