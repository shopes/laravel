<?php
/**
 * @since 2017/1/8
 */

namespace App\Console\Commands\Dev;


use App\Console\Commands\DevCommand;
use Illuminate\Console\Command;

class Otpauth extends DevCommand
{
    public function cmd()
    {
        $param = [
            "chs" => '400x400',
            "chld" => "M|0",
            "cht" => 'qr',
            "chl" => 'otpauth://hotp/WeiPay:user@shopes.cn?secret='.$this->create_secret().'&issuer=WeiPay&counter='.time(),
        ];
        echo "https://www.google.com/chart?".http_build_query($param);
        
    }
    
    function create_secret()
    {
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567'; // allowed characters in Base32
        $secret = '';
        for($i = 0; $i < 16; $i++) {
            $secret .= substr($chars, wp_rand(0, strlen($chars) - 1), 1);
        }
        return $secret;
    }
}