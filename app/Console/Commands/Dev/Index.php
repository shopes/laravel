<?php
/**
 * @since 2017/1/8
 */

namespace App\Console\Commands\Dev;


use App\Console\Commands\DevCommand;
use Illuminate\Console\Command;
use Modules;

class Index extends DevCommand
{
    public function cmd_index()
    {
        foreach(Modules::all() as $module) {
           // k($module->getPath("Database/Schema"));
            if(is_dir($module->getPath("Database/Schema"))) {
                k($module->getName());
                //$this->info($module->getName()) ;
            }
        }
      $this->info('test');
    }
}