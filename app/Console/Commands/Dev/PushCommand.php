<?php
/**
 * @since 2017/1/8
 */

namespace App\Console\Commands\Dev;


use App\Console\Commands\DevCommand;
use Cache;

class PushCommand extends DevCommand
{
    public function cmd_push($op = null)
    {

        //$this->hasArgument('act') && $op = $this->argument('act');
        date_default_timezone_set('Asia/Shanghai');
        
        $cache = Cache::get('app:version', []);
        $marjor = date("Y") - 2016;//主版本年份
        $minor = date('n');//月份，没有前导0
        //$patch = date('W');//星期和星期中的第几天
        $date = Date('Y-m-d');
        $week = intval(strftime("%U", strtotime($date)));
        
        $cache[date("Y") . $week] += 1;
        
        //$patch = sprintf("%02s", $cache[date("Y") . $week]);
        $patch = $cache[date("Y") . $week];
        //$patch = $week. $patch;
        $ver = "ver: $marjor.{$minor}.{$week}$patch ";
        system('git add -A');
        exec('git commit -m "' . addslashes($ver . $op) . '"', $output);
        $output = join(PHP_EOL, $output);
        //var_dump($output);
        //kd(strpos($output, 'nothing'));
        if(strpos($output, 'nothing') === false) {
            Cache::forever('app:version', $cache);
            system("git push origin --all");
            if($op == 'all'){
               system("git push bs --all");

            }
        }
        system("git --no-pager log --pretty=oneline -n 1 --stat --abbrev-commit");
    }
}