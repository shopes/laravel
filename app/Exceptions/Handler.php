<?php

namespace App\Exceptions;

use App;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Access\Response;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use View;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        AuthorizationException::class,
        HttpException::class,
        NotFoundHttpException::class,
        MethodNotAllowedHttpException::class,
        
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];
    
    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }
    
    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function _render($request, Exception $exception)
    {
//        if($this->shouldntReport($exception)) {
//            return \Response::make('');
//        }
        return parent::render($request, $exception);
    }
    
    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     * @return \Illuminate\Http\Response|string
     */
    public function render($request, Exception $exception)
    {
        
//        if($exception instanceof MethodNotAllowedHttpException || $exception instanceof NotFoundHttpException) {
//            return \Response::make('');
//
//        }
        if ($exception instanceof NotFoundHttpException)
        {
            // Your stuff here
            return "";// \Response::make('') ;
        }
        
        //return parent::render($request, $exception);
        $e = $exception;
        if($this->isHttpException($e)) {
            /** @var $e HttpException; */
            switch($e->getStatusCode()) {
                case '404':
                    
                    //\Log::error($e);
                    //return \Response::view('custom.404');
                    //return \App::make(SystemController::class)->all404();
                    return \Response::make('');
                    break;
                
                case '500':
                    \Log::error($exception);
                    return \Response::view('custom.500');
                    break;
                
                default:
                    return parent::render($request, $e);
                    break;
            }
        } else {
            return parent::render($request, $e);
        }
        
    }
    
    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Illuminate\Auth\AuthenticationException $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }
        
        return redirect()->guest('login');
    }
}
