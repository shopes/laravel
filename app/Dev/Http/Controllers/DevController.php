<?php namespace App\Dev\Http\Controllers;

use Homeshop\Modules\Routing\Controller;

class DevController extends Controller {
	
	public function index()
	{
		return view('dev::index');
	}
	
}