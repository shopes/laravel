
#ifdef HAVE_CONFIG_H
#include "../ext_config.h"
#endif

#include <php.h>
#include "../php_ext.h"
#include "../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/file.h"
#include "kernel/concat.h"
#include "kernel/string.h"


ZEPHIR_INIT_CLASS(Poem_People) {

	ZEPHIR_REGISTER_CLASS(Poem, People, poem, people, poem_people_method_entry, 0);

	return SUCCESS;

}

PHP_METHOD(Poem_People, say) {

	zval *a = NULL, *_0 = NULL, *auth = NULL, _1, *output = NULL, *_3, *hash = NULL, _4, _5, _6, _7, *_8, _2$$3;
	int ZEPHIR_LAST_CALL_STATUS;

	ZEPHIR_MM_GROW();

	ZEPHIR_INIT_VAR(a);
	ZVAL_STRING(a, "", 1);
	ZEPHIR_INIT_VAR(_0);
	ZVAL_STRING(_0, "dmidecode --type=1", ZEPHIR_TEMP_PARAM_COPY);
	ZEPHIR_MAKE_REF(a);
	ZEPHIR_CALL_FUNCTION(NULL, "exec", NULL, 1, _0, a);
	zephir_check_temp_parameter(_0);
	ZEPHIR_UNREF(a);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(auth);
	ZVAL_STRING(auth, "", 1);
	ZEPHIR_SINIT_VAR(_1);
	ZVAL_STRING(&_1, "/var/smallauth", 0);
	if ((zephir_file_exists(&_1 TSRMLS_CC) == SUCCESS)) {
		ZEPHIR_SINIT_VAR(_2$$3);
		ZVAL_STRING(&_2$$3, "/var/smallauth", 0);
		ZEPHIR_INIT_NVAR(auth);
		zephir_file_get_contents(auth, &_2$$3 TSRMLS_CC);
	}
	ZEPHIR_INIT_NVAR(_0);
	zephir_fast_join_str(_0, SL("\n\n"), a TSRMLS_CC);
	ZEPHIR_INIT_VAR(_3);
	ZEPHIR_CONCAT_VV(_3, _0, auth);
	ZEPHIR_CALL_FUNCTION(&output, "sha1", NULL, 2, _3);
	zephir_check_call_status();
	ZEPHIR_SINIT_VAR(_4);
	ZVAL_STRING(&_4, "sha256", 0);
	ZEPHIR_SINIT_VAR(_5);
	ZVAL_STRING(&_5, "moshihui@gmail.com", 0);
	ZEPHIR_SINIT_VAR(_6);
	ZVAL_LONG(&_6, 52013);
	ZEPHIR_SINIT_VAR(_7);
	ZVAL_LONG(&_7, 32);
	ZEPHIR_CALL_FUNCTION(&hash, "hash_pbkdf2", NULL, 3, &_4, output, &_5, &_6, &_7);
	zephir_check_call_status();
	ZEPHIR_INIT_VAR(_8);
	zephir_fast_strtoupper(_8, hash);
	zend_print_zval(_8, 0);
	ZEPHIR_MM_RESTORE();

}

