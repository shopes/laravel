
extern zend_class_entry *poem_people_ce;

ZEPHIR_INIT_CLASS(Poem_People);

PHP_METHOD(Poem_People, say);

ZEPHIR_INIT_FUNCS(poem_people_method_entry) {
	PHP_ME(Poem_People, say, NULL, ZEND_ACC_PUBLIC|ZEND_ACC_STATIC)
	PHP_FE_END
};
