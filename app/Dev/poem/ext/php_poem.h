
/* This file was generated automatically by Zephir do not modify it! */

#ifndef PHP_POEM_H
#define PHP_POEM_H 1

#ifdef PHP_WIN32
#define ZEPHIR_RELEASE 1
#endif

#include "kernel/globals.h"

#define PHP_POEM_NAME        "poem"
#define PHP_POEM_VERSION     "0.0.1"
#define PHP_POEM_EXTNAME     "poem"
#define PHP_POEM_AUTHOR      ""
#define PHP_POEM_ZEPVERSION  "0.9.6a-dev-72dbb2063e"
#define PHP_POEM_DESCRIPTION ""



ZEND_BEGIN_MODULE_GLOBALS(poem)

	int initialized;

	/* Memory */
	zephir_memory_entry *start_memory; /**< The first preallocated frame */
	zephir_memory_entry *end_memory; /**< The last preallocate frame */
	zephir_memory_entry *active_memory; /**< The current memory frame */

	/* Virtual Symbol Tables */
	zephir_symbol_table *active_symbol_table;

	/** Function cache */
	HashTable *fcache;

	zephir_fcall_cache_entry *scache[ZEPHIR_MAX_CACHE_SLOTS];

	/* Cache enabled */
	unsigned int cache_enabled;

	/* Max recursion control */
	unsigned int recursive_lock;

	/* Global constants */
	zval *global_true;
	zval *global_false;
	zval *global_null;
	
ZEND_END_MODULE_GLOBALS(poem)

#ifdef ZTS
#include "TSRM.h"
#endif

ZEND_EXTERN_MODULE_GLOBALS(poem)

#ifdef ZTS
	#define ZEPHIR_GLOBAL(v) TSRMG(poem_globals_id, zend_poem_globals *, v)
#else
	#define ZEPHIR_GLOBAL(v) (poem_globals.v)
#endif

#ifdef ZTS
	#define ZEPHIR_VGLOBAL ((zend_poem_globals *) (*((void ***) tsrm_ls))[TSRM_UNSHUFFLE_RSRC_ID(poem_globals_id)])
#else
	#define ZEPHIR_VGLOBAL &(poem_globals)
#endif

#define ZEPHIR_API ZEND_API

#define zephir_globals_def poem_globals
#define zend_zephir_globals_def zend_poem_globals

extern zend_module_entry poem_module_entry;
#define phpext_poem_ptr &poem_module_entry

#endif
