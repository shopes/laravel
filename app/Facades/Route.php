<?php
/**
 * Date: 2016/12/21
 */

namespace App\Facades;


use App;

class Route extends \Illuminate\Support\Facades\Route
{
    /**
     * Wildcard routing helper
     * @example
     * // example setup
     * Route::controller('test', 'TestController');
     *
     * // example routes
     * http://localhost/test/foo
     * http://localhost/test/bar/1
     * http://localhost/test/baz/1/2/3
     *
     * @param string $route Base route, i.e. 'test'
     * @param string $controller Path to controller, i.e. 'TestController'
     * @param string|array $verbs Optional route verb(s), i.e. 'get'; defaults to ['get', 'post']
     */
    public static function controller($route, $controller, $verbs = ['get', 'post'])
    {
        // variables
        if($controller instanceof \Closure) {
            $controller = $controller();
        }
        $stack = \Route::getGroupStack();
        $namespace = end($stack)['namespace'];
        if(!class_exists($controller)) {
            $controller = $namespace . '\\' . $controller;
        }
        $action = "";
        // routing
        \Route::match($verbs, rtrim($route, '/') . '/{method?}/{params?}',
            function($method = 'index', $params = null) use ($route, $controller, $namespace, $action) {
                if(method_exists($controller, \Request::getMethod() . $method)) {
                    $method = strtolower(\Request::getMethod()) . $method;
                }
                $action = $method;
                
                $method = $controller . '@' . $method;
                
                return $params
                    ? App::call($method, explode('/', $params))
                    : App::call($method);
            })->where('params', '.*')->name(trim(str_replace('/', '.', $route) . ".$action", "."));
    }
}