<?php
/**
 * Date: 2017/1/5
 */

namespace App\Facades;

use Illuminate\Support\Facades\Facade;


/**
 * @see \base_cache_manager
 */
class Setting extends Facade
{
    /**
     * The cache manager instance
     *
     * @var \base_cache_manager
     */
    private static $__cache;
    
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        if (!static::$__cache)
        {
            static::$__cache = new \base_cache_manager;
        }
        return static::$__cache;
    }
}
