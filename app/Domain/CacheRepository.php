<?php
/**
 * Created by PhpStorm.
 * User: Mo
 * Date: 2016/12/11
 * Time: 下午11:23
 */

namespace App\Domain;


use App;
use Illuminate\Cache\Repository;
use Illuminate\Support\Arr;

class CacheRepository extends Repository
{
    /**
     * @param $namespace
     * @return \App\Custom\\CacheRepository
     */
    public function app($namespace = 'app/core')
    {
        static $cache = array();
        if(!isset($cache[$namespace])) {
            $cache[$namespace] = new CacheInstance($this, $namespace);
        }
        return $cache[$namespace];
    }

    /**
     * Retrieve an item from the cache by key.
     *
     * @param  string $key
     * @param  mixed $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
    
        error_reporting(error_reporting() & ~E_NOTICE);
    
    
    
        if(is_array($key)) {
            return $this->many($key);
        }

        list($key, $sub) = explode('.', $key, 2);

        $value = $this->store->get($this->itemKey($key));

        if(is_array($value) && isset($sub)) {
            $value = array_get($value, $sub);
        }

        if(is_null($value)) {
            $this->fireCacheEvent('missed', [$key]);

            $value = value($default);
        } else {
            $this->fireCacheEvent('hit', [$key, $value]);
        }

        return $value;
    }

    public function encrypt($key, $value)
    {
        return $this->crypt($key, $value, true);
    }

    public function decrypt($key, $default = null)
    {
        return $this->crypt($key, $default, false);
    }

    private function crypt($key, $value, $encode = false)
    {
        static $storage = [];

        list($namespace, $key) = explode('.', $key, 2);
        if(empty($key)) {
            $key = $namespace;
            $namespace = 'app/core';
        }

    }

    private $rc4_password;

    /*
     * rc4加密算法
     * $pwd 密钥
     * $data 要加密的数据
     */
    function rc4($data, &$name, $pwd = null)//$pwd密钥　$data需加密字符串
    {
        if(empty($pwd) && is_string($this->rc4_password) && !empty($this->rc4_password)) {
            $pwd = $this->rc4_password;
            $this->rc4_password = null;
        }
        if (version_compare(phpversion(), '5.6.0', '<') || !function_exists('hash_pbkdf2')) {
            exit('php version isn\'t high enough');
        }

        $key[] = "";
        $box[] = "";
        $pwd = hash_pbkdf2('sha256',md5(md5($pwd).$name),md5($name.md5($pwd)),10000,256);
        $name = md5(strrev(md5($pwd)).md5($name));
        if(is_null($data)){
            $data = $this->get($name);
        }
        if(is_object($data) || is_array($data)){
            $data = serialize($data);
        }

        $pwd_length = strlen($pwd);
        $data_length = strlen($data);

        for($i = 0; $i < 256; $i++) {
            $key[$i] = ord($pwd[$i % $pwd_length]);
            $box[$i] = $i;
        }

        for($j = $i = 0; $i < 256; $i++) {
            $j = ($j + $box[$i] + $key[$i]) % 256;
            $tmp = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }
        $cipher = '';
        for($a = $j = $i = 0; $i < $data_length; $i++) {
            $a = ($a + 1) % 256;
            $j = ($j + $box[$a]) % 256;

            $tmp = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;

            $k = $box[(($box[$a] + $box[$j]) % 256)];
            $cipher .= chr(ord($data[$i]) ^ $k);
        }

        if(is_serialized($cipher)){
            try {
                $cipher = @unserialize($cipher);
            } catch(\Exception $e){
                $cipher = false;
            }
        }

        return $cipher;
    }

}