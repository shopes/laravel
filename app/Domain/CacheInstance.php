<?php
/**
 * Created by PhpStorm.
 * User: Mo
 * Date: 2016/12/12
 * Time: 上午12:15
 */

namespace App\Domain;


class CacheInstance
{
    private $repository;
    private $namespace;

    public function __construct(CacheRepository $repository, $namespace)
    {
        $this->repository = $repository;
        $this->namespace = $namespace;
    }

    public function __call($name, $arguments)
    {
        // TODO: Implement __call() method.
        if(is_string($arguments[0])){
            $arguments[0] = $this->namespace.'.'.$arguments['0'];
        }
        return call_user_func_array([$this->repository,$name],$arguments);
    }

}