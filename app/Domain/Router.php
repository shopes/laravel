<?php
/**
 * @since 2017/1/8
 */

namespace App\Domain;


use App;
use Illuminate\Routing\Router as BaseRouter;

class Router extends BaseRouter
{
    
    /**
     * Wildcard routing helper
     * @example
     * // example setup
     * Route::controller('test', 'TestController');
     *
     * // example routes
     * http://localhost/test/foo
     * http://localhost/test/bar/1
     * http://localhost/test/baz/1/2/3
     *
     * @param string $route Base route, i.e. 'test'
     * @param string $controller Path to controller, i.e. 'TestController'
     * @param string|array $verbs Optional route verb(s), i.e. 'get'; defaults to ['get', 'post']
     */
    public function controller($route, $controller, $verbs = ['get', 'post'])
    {
        // variables
        $stack = \Route::getGroupStack();
        $namespace = end($stack)['namespace'];
        $controller = $namespace . '\\' . $controller;
        $action = "";
        // routing
        \Route::match($verbs, rtrim($route, '/') . '/{method}/{params?}',
            function($method, $params = null) use ($route, $controller, $namespace, $action) {
                $action = $method;
                $method = $controller . '@' . $method;
                $params
                    ? App::call($method, explode('/', $params))
                    : App::call($method);
            })->where('params', '.*')->name($route . ($action ? ".$action" : ''));
    }
}
