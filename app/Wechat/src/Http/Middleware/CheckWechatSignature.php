<?php

namespace Wechat\Http\Middleware;

use \Closure;
use Illuminate\Http\Request;

class CheckWechatSignature
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->has('signature') || !$request->has('timestamp') || !$request->has('nonce')) {
            throw new \Exception(trans('wechat.illegal_access'));
        }

        $signature = $request->input('signature');
        $timestamp = $request->input('timestamp');
        $nonce = $request->input('nonce');

        $app = wechat($request->uniqid);

        if (! $app->checkSignature($signature, $timestamp, $nonce)) {
            throw new \Exception(trans('wechat.illegal_access'));
        }

        return $next($request);
    }
}