<?php

namespace Wechat\Http\Controllers;

use Log;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

/**
 * 微信控制器，负责微信的接入验证和微信主动推送消息的处理
 *
 */
class WechatController extends Controller
{
    
    /**
     * GET请求，视为接入验证
     *
     * @param  Request $request 请求
     * @param  [type]  $uniqid  应用的唯一标识，为空时默认为配置中得第一个
     * @return array|string [type]           响应内容
     */
    public function getAccess(Request $request, $uniqid = null)
    {
       
        // 校验通过后直接返回请求中的echostr
        return $request->input('echostr');
    }
    
    protected function log($data)
    {
        if(is_array($data) || is_object($data)) {
            $data = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        }
        file_put_contents(storage_path("logs/wechat.log"), $data, FILE_APPEND);
    }
    
    /**
     * POST请求，由微信主动推送的消息
     *
     * @param  Request $request 请求
     * @param  [type]  $uniqid  应用的唯一标识，为空时默认为配置中得第一个
     * @return [type]           返回给微信的消息
     */
    public function postAccess(Request $request, $uniqid = null)
    {
        
        $message = $request->getContent();
        
        if(empty($message)) {
            echo trans('wechat.data_error');
            exit;
        }
        
        $app = wechat($uniqid);
        
        // 找不到uniqid的应用，终止执行
        if(is_null($app)) {
            echo trans('wechat.wechat_not_available');
            exit;
        }
        
        Log::info("{$uniqid} receve message : {$message}");
        
        $message = simplexml_load_string($message, 'SimpleXMLElement', LIBXML_NOCDATA);
        return response($app->handle($message));
    }
}
