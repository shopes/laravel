<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Route;// Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Upgate\LaravelJsonRpc\Contract\ServerInterface as JsonRpcServerContract;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }
    
    /**
     * Define the routes for the application.
     *
     * @param Router $router
     * @return void
     */
    public function map(Router $router)
    {
        $this->mapApiRoutes($router);

        $this->mapWebRoutes($router);

        //
    }
    
    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param Router $router
     * @return void
     */
    protected function mapWebRoutes(Router $router)
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            //kd(get_class($router));
            require base_path('routes/web.php');
        });
    }
    
    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @param Router $router
     * @return void
     */
    protected function mapApiRoutes(Router $router)
    {
        Route::group([
            'middleware' => 'api',
            'namespace' => $this->namespace,
            'prefix' => 'api',
        ], function (Router $router) {
            
            // Create an instance of JsonRpcServer
            $jsonRpcServer = $this->app->make(JsonRpcServerContract::class);
            // Set default controller namespace
            $jsonRpcServer->setControllerNamespace($this->namespace);
            // Register middleware aliases configured for Laravel router
            $jsonRpcServer->registerMiddlewareAliases($router->getMiddleware());
            
            require base_path('routes/api.php');
        });
        Route::group([
            'middleware' => 'wechat',
            'namespace' => $this->namespace,
            'prefix' => 'wechat',
        ], function (Router $router) {
            require base_path('routes/wechat.php');
        });
    }
}
