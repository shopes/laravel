<?php

namespace App\Providers;

use App\Console\Commands\DevCommand;
use App\Console\Commands\UpdateCommand;
use Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        error_reporting(error_reporting() & ~E_NOTICE);
    
        //
        $this->commands([
            DevCommand::class,
            UpdateCommand::class,
        ]);
    
        Blade::directive('wpposts', function ($params) {
            return '<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>';
        });
    
        Blade::directive('wpquery', function ($params) {
            $replacement = "<?php \$bladequery = new WP_Query($params); ";
            $replacement .= 'if ( $bladequery->have_posts() ) : ';
            $replacement .= 'while ( $bladequery->have_posts() ) : ';
            $replacement .= '$bladequery->the_post(); ?> ';
            return $replacement;
        });
    
        Blade::directive('wpempty', function () {
            return '<?php endwhile; ?><?php else: ?>';
        });
    
        Blade::directive('wpend', function () {
            return '<?php endif; wp_reset_postdata(); ?>';
        });
    
        Blade::directive('debug', function ($params = null) {
            return "<?php kd($params); ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        error_reporting(error_reporting() & ~E_NOTICE);
    }
}
