<?php
/**
 * Created by PhpStorm.
 * User: Mo
 * Date: 2016/12/11
 * Time: 下午11:15
 */

namespace App\Providers;


use App\Domain\CacheManager;
use Illuminate\Cache\MemcachedConnector;

class CacheServiceProvider extends \Illuminate\Cache\CacheServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    
        $this->app->singleton('cache', function ($app) {
            return new CacheManager($app);
        });

        $this->app->singleton('cache.store', function ($app) {
            return $app['cache']->driver();
        });

        $this->app->singleton('memcached.connector', function () {
            return new MemcachedConnector;
        });

        $this->registerCommands();
    }
}