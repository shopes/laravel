<?php namespace App\Base\Http\Controllers;

use Homeshop\Modules\Routing\Controller;

class BaseController extends Controller {
	
	public function index()
	{
		return view('base::index');
	}
	
}