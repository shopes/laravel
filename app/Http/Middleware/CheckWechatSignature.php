<?php

namespace App\Http\Middleware;

use \Closure;
use Illuminate\Http\Request;

class CheckWechatSignature
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle(Request $request, Closure $next)
    {
        //wechat()->log($request->all());
    
        if(!$request->has('signature') || !$request->has('timestamp') || !$request->has('nonce')) {
            $this->log('wechat.lost_params');
            throw new \Exception(trans('wechat.lost_params'));
        }
        
        $signature = $request->input('signature');
        $timestamp = $request->input('timestamp');
        $nonce = $request->input('nonce');
        $app = wechat($request->uniqid);
        
        if(!$app->checkSignature($signature, $timestamp, $nonce)) {
            wechat()->log('illegal_access');
            throw new \Exception(trans('wechat.illegal_access'));
        }
        
        return $next($request);
    }
    
    protected function log($data)
    {
        if(is_array($data) || is_object($data)) {
            $data = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        }
        file_put_contents(storage_path("logs/wechat.log"), $data, FILE_APPEND);
    }
}