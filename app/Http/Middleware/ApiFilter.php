<?php
/**
 * @since 2017/1/8
 */

namespace App\Http\Middleware;


use App;
use Closure;

class ApiFilter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        App::stop(true);
        
        return $next($request);
    }
}