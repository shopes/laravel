<?php
/**
 * Date: 2017/1/5
 */

namespace App;


use Blade;
use Illuminate\Contracts\Http\Kernel as HttpKernel;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\View\View;

class Kernel extends Application
{
    public $_app_instance = [];

    /** @var  Kernel */
    protected static $_app;

    /**
     * @param $appid
     * @return CoreManager
     */
    public function get($appid)
    {
        if (!isset($this->_app_instance[$appid])) {
            $this->_app_instance[$appid] = new CoreManager($appid);
        }
        return $this->_app_instance[$appid];
    }

    protected static $stop = false;

    public static function stop($value = null)
    {
        return isset($value) ? self::$stop = $value : self::$stop;
    }

    /**
     * @param Kernel $app
     * @return Kernel
     */
    public static function init(Kernel $app)
    {
        return self::$_app ?: self::$_app = $app;
    }

    /** @var  \Illuminate\View\View */
    public static $view;

    public function run()
    {

        add_filter('the_content', function ($__content) {
            ob_start();
            global $__env;
            extract(\View::getShared(), EXTR_SKIP);
            extract($GLOBALS, EXTR_SKIP);
            eval('?>' . Blade::compileString($__content));
            return ob_get_clean();
        });

        add_filter('template_include', function ($template) {
            $name = substr(realpath($template), strlen(get_template_directory()) + 1);
            $name = str_replace(['.php', '/'], ['', '.'], $name);
            if (\View::exists($viewName = 'wp::' . $name)) {
                //kd($name);
                /** @var View $view */
                //$GLOBALS['__env'] = View::shared('__env');
                $view = \View::make($viewName);
                $data = $view->getData();
                if (Kernel::$view instanceof View) {
                    $data = array_merge($data, Kernel::$view->getData());

                    //$data = array_merge($data, );
                }
                foreach (array_merge(\View::getShared(), $data) as $k => $v) {
                    $GLOBALS[$k] = $v;
                }
                if (self::$view instanceof View) {
                    foreach (self::$view->renderSections() as $k => $v) {
                        $GLOBALS['__env']->inject($k, $v);
                    }
                }
                //$GLOBALS = array_merge($GLOBALS,$data);
                if (Blade::isExpired($view->getPath()) || filemtime($template) > Blade::getCompiledPath($view->getPath())) {
                    Blade::compile($view->getPath());
                }

                $template = Blade::getCompiledPath($view->getPath());


            }
            return $template;
        });


        $kernel = self::app()->make(\Illuminate\Contracts\Http\Kernel::class);

        $response = $kernel->handle(
            $request = Request::capture()
        );




        if ($isLaravelOutput = is_object($response) && method_exists($response, 'send')) {

            self::$view = $response->original;

            $response->send();
//            if(PHP_SAPI != 'cli'){
//                kd('test');
//
//            }

        } else {
            echo(empty($response) || is_string($response) || is_numeric($response)
                ? $response
                : json_encode($response, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
        }

        //kd(\Kernel::basePath());

        $kernel->terminate($request, $response);

        if ($isLaravelOutput || self::stop()) {
            exit;
        } else {

        }

    }

    /**
     * @return Kernel
     */
    public static function app()
    {
        return self::$_app;
    }


}