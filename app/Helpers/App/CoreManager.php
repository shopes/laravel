<?php
/**
 * Date: 2017/1/5
 */

namespace App;


use App;
use base_database_manager;

class CoreManager extends \ArrayObject
{
    public $app_id;
    public $app_dir;
    
    public function __construct($appid)
    {
        parent::__construct([]);
        $this->app_id = $appid;
        $this->app_dir = substr(\Modules::getModulePath($appid), 0, -1);
    }
    
    
    public function model($model)
    {
        return \App::make($this->app_id . '_' . $model);
    }
    
    protected $db;
    
    /**
     * @return \Doctrine\DBAL\Connection
     */
    public function database()
    {
        return $this->db = $this->db ?: App::make(base_database_manager::class)->connection();
        
    }
    
    public function _($str)
    {
        return $str;
    }
    
    public function get_parent_model_class()
    {
        $parent_model_class = $this->define('parent_model_class');
        return $parent_model_class ? $parent_model_class : 'dbeav_model';
    }
    
    public function define($path = null)
    {
        if(!$this->__define) {
            if(is_dir($this->app_dir) && file_exists($this->app_dir . '/app.xml')) {
                $tags = array();
                $file_contents = file_get_contents($this->app_dir . '/app.xml');
                $this->__define = App::singleton('base_xml')->xml2array(
                    $file_contents, 'base_app');
            } else {
                $row = app::get('base')->model('apps')->getList('remote_config', array('app_id' => $this->app_id));
                $this->__define = $row[0]['remote_config'];
            }
        }
        if($path) {
            return eval('return $this->__define[' . str_replace('/', '][', $path) . '];');
        } else {
            return $this->__define;
        }
    }
    
    public function getConf($name, $default = null)
    {
        return \Cache::get($this->app_id . ':' . $name, $default);
    }
    
    public function setConf($name, $value)
    {
        return \Cache::forever($this->app_id . ':' . $name, $value);
    }
}