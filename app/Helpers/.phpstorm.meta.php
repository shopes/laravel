<?php
namespace PHPSTORM_META {
    
    /**
     * PhpStorm Meta file, to provide autocomplete information for PhpStorm
     * Generated on 2017-01-04.
     *
     * @author Barry vd. Heuvel <barryvdh@gmail.com>
     * @see https://github.com/barryvdh/laravel-ide-helper
     */
    $STATIC_METHOD_TYPES = [
        \App::singleton('') => [
            '' == '@',
        ],
    ];
}