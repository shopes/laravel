<?php

use Mdanter\Ecc\EccFactory;
use Mdanter\Ecc\Crypto\Signature\Signer;
use Mdanter\Ecc\Math\GmpMathInterface;
use Mdanter\Ecc\Serializer\Signature\DerSignatureSerializer;
use Mdanter\Ecc\Serializer\PublicKey\DerPublicKeySerializer;
use Mdanter\Ecc\Serializer\PublicKey\PemPublicKeySerializer;
use Mdanter\Ecc\Serializer\PrivateKey\PemPrivateKeySerializer;
use Mdanter\Ecc\Serializer\PrivateKey\DerPrivateKeySerializer;


/**
 * fcache
 * 最大bs 16m
 * 最多16个类型
 *
 * @package
 * @version $Id: secache.php 34670 2009-10-28 10:42:14Z ever $
 * @copyright 2003-2007 ShopEx
 * @author Wanglei <flaboy@shopex.cn>
 * @license Commercial
 * [20][20][384][16*16*16*4]
 */
class base_cache_store_secacheEngine
{
    
    var $idx_node_size = 40;
    var $data_core_pos = 262588; //40+20+24*16+16*16*16*16*4;
    var $schema_item_size = 24;
    var $header_padding = 20; //保留空间 放置php标记防止下载
    var $info_size = 20; //保留空间 4+4+12 maxsize|ver|root_key
    
    //40起 添加20字节保留区域
    var $idx_seq_pos = 40; //id 计数器节点地址
    var $dfile_cur_pos = 44; //id 计数器节点地址
    var $idx_free_pos = 48; //id 空闲链表入口地址
    
    var $idx_core_pos = 444; //40+20+24*16
    var $min_size = 10240; //10M最小值
    var $schema_struct = array('size', 'free', 'lru_head', 'lru_tail', 'hits', 'miss');
    var $ver = '$Rev: 34670 $';
    var $name = 'secache';
    public $_block_size_list;
    public $_bsize_list;
    public $_rs;
    public $max_size;
    public $_node_struct;
    public $idx_node_base;
    public $_in_fatal_error;
    private $_file = null;
    
    var $secache_size = '1g';
    
    var $config = [];
    
    const INTERNAL_ROOT_KEY = 0;
    private $keyEncrypt;
    
    public $namespace = ['inter', 'user', 'app', 'cache', 'tmp', 'remote', 'addon'];
    
    /**
     * @param $workat
     * @param $file
     * @param string $size
     * @param array $config
     * @return static
     */
    public static function instance($workat, $file, $size = '1g', $config = [])
    {
        static $instance = [];
        if(empty($instance[$path = md5($workat . '/' . $file)])) {
            $instance[$path] = new static($workat, $file, $size, $config);
        }
        return $instance[$path];
    }
    
    private function __construct($workat, $file, $size = '1g', $config = [])
    {
        
        if(version_compare(PHP_VERSION, '5.6', '<')) {
            throw  new Exception('PHP Version must than 5.6');
            return;
        }
        
        foreach(['mcrypt', 'gmp'] as $extension) {
            if(!extension_loaded($extension)) {
                throw  new Exception($extension . ' extension must installed');
                return;
            }
        }
        
        
        //$workat = DATA_DIR . '/cache';
        //$this->secache_size = config::get('cache.base_cache_secache.size', '15M');
        $this->secache_size = $size;
        if(!is_dir($workat)) {
            utils::mkdir_p($workat);
        }
        
        $this->config = $config;
        
        $workat = $workat . '/' . $file . ".php";
        //$this->workat($workat . '/secache');
        $this->workat($workat);
    }
    
    /**
     * RC4 加解密
     * @param $string
     * @param bool $decrypt
     * @param string $key
     * @return mixed|string
     * @throws Exception
     */
    private function encrypt($string, $decrypt = false, $key = '')
    {
        $error_reporting = error_reporting();
        error_reporting(0);
        static $defaultKey = null;
        if(is_null($string) && !empty($this->root_key) && empty($defaultKey)) {
            try {
                $defaultKey = hash_pbkdf2("sha256", md5($this->root_key), $this->ver, 10086, 32);
            } catch(Exception $e) {
                throw new Exception('encrypt error');
            }
        }
        $key = md5(($key ?: $this->keyEncrypt) . $defaultKey);
        $this->keyEncrypt = false;
        $key_length = strlen($key);
        $string = $decrypt ? base64_decode(preg_replace("/\s+/s", '', $string)) : substr(md5(serialize($string) . $key),
                0, 8) . serialize($string);
        $string_length = strlen($string);
        $rndkey = $box = array();
        $result = '';
        for($i = 0; $i <= 255; $i++) {
            $rndkey[$i] = ord($key[$i % $key_length]);
            $box[$i] = $i;
        }
        for($j = $i = 0; $i < 256; $i++) {
            $j = ($j + $box[$i] + $rndkey[$i]) % 256;
            $tmp = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }
        for($a = $j = $i = 0; $i < $string_length; $i++) {
            $a = ($a + 1) % 256;
            $j = ($j + $box[$a]) % 256;
            $tmp = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;
            $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
        }
        if($decrypt) {
            if(substr($result, 0, 8) == substr(md5(substr($result, 8) . $key), 0, 8)) {
                $return = @unserialize(substr($result, 8));
            } else {
                $return = '';
            }
        } else {
            $return = str_replace('=', '', base64_encode($result));
        }
        error_reporting($error_reporting);
        return $return;
    }
    
    
    private function workat($file)
    {
        //$this->_file = $file.'.php';
        $this->_file = $file;
        $this->_bsize_list = array(
            512 => 10,
            3 << 10 => 10,
            8 << 10 => 10,
            20 << 10 => 4,
            30 << 10 => 2,
            50 << 10 => 2,
            80 << 10 => 2,
            96 << 10 => 2,
            128 << 10 => 2,
            224 << 10 => 2,
            256 << 10 => 2,
            512 << 10 => 1,
            1024 << 10 => 1,
        );
        
        $this->_node_struct = array(
            'next' => array(0, 'V'),
            'prev' => array(4, 'V'),
            'data' => array(8, 'V'),
            'size' => array(12, 'V'),
            'lru_right' => array(16, 'V'),
            'lru_left' => array(20, 'V'),
            'key' => array(24, 'H*'),
        );
        if(!file_exists($this->_file)) {
            $this->create();
        } else {
            $this->_rs = fopen($this->_file,
                'rb+') or $this->trigger_error('Can\'t open the cachefile: ' . realpath($this->_file), E_USER_ERROR);
            $this->_seek($this->header_padding);
            if(version_compare(phpversion(), '5.5', 'gt')) {
                $info = unpack('V1max_size/Z*ver', fread($this->_rs, $this->info_size));
            } else {
                $info = unpack('V1max_size/a*ver', fread($this->_rs, $this->info_size));
            }
            
            
            if(substr($info['ver'], 0, 4) != substr(md5($this->ver), 0, 4)) {
                $this->_format(true);
            } else {
                $this->max_size = $info['max_size'];
                $this->root_key = substr($info['ver'], 4);
                $this->encrypt(null);
            }
        }
        
        
        $this->idx_node_base = $this->data_core_pos + $this->max_size;
        $this->_block_size_list = array_keys($this->_bsize_list);
        sort($this->_block_size_list);
        
        $this->ECDSA();
        
        return true;
    }
    
    private function ECDSA()
    {
        static $dsaPubKey = null;
        if(!isset($dsaPubKey)) {
            $thekey = hash_pbkdf2('sha256', 1483900130, $this->ver, 12380);
            if(env('INIT_PUBKEY')) {
                $pubkey = $this->config['pubkey'];
                $enc = chunk_split(base64_encode($this->encrypt($pubkey, false, $thekey)), 64);
                $dec = $this->encrypt(base64_decode($enc), true);
                kd($pubkey, $enc, $dec);
            }
            $keystr = <<<KEY
bzRqVFBKMGxINFVBNFdYeE44SFl6ZTllN2RuRG1sVlorV2hFVjhlVnh5UVp6ZFpV
WlZVQUJHZFcvWkg2WjNHSndjN3N6RnBBS3d2c05BMUhMYkdWV3d2cWU0NnNmcDRT
bloxdDh5Z2NRYzFRdXRmN0hsWVkwNkhLUzhsekIyVFhHTHJuWmNGSXFqc1NOV2sy
a3NsektlcU9WRmNKWThrNUtxdFA0RTBHQlVjN2xUOFZSdWprekFFT3B0cm9ONVFL
Ty8zdTZJU3NpdGYwaWVBZGF2ZkpJRXUzRzhOV0Ribmg5V0hMQ2VvdUozSDN2bDNq
OUpNRGtmT3IzelBOdGwzOWt4NEJLb3Vsc3NxSzNNRGFLZStBUmN0WDlKRGRqWEVF
cHNUd3c0aklVaVZxdkhDUXZaWDBHdVF5
KEY;
            $this->keyEncrypt = $thekey;
            $dsaPubKey = $this->encrypt(base64_decode($keystr), true);
        }
        
        $inter_key = $this->fetch("inter:ECDSA_KEY");
        if(empty($inter_key)) {
            $inter_key = [];
            $adapter = EccFactory::getAdapter();
            $generator = EccFactory::getNistCurves()->generator256();
            $private = $generator->createPrivateKey();
            
            $keySerializer = new PemPrivateKeySerializer(new DerPrivateKeySerializer($adapter));
            $data = $keySerializer->serialize($private);
            //echo $data . PHP_EOL;
            $inter_key['private'] = $data;
            $pubKey = $private->getPublicKey();
            $keySerializer = new PemPublicKeySerializer(new DerPublicKeySerializer($adapter));
            
            $data = $keySerializer->serialize($pubKey);
            //echo $data . PHP_EOL;
            $inter_key['public'] = $data;
            $this->ecc_sign = [serialize($inter_key), $inter_key['private']];
            $sign = $this->sign();
            $this->verisign_data = [self::ENCRYPT_INTER => $inter_key['public']];
            $this->verisign();
            $this->store("inter:ECDSA_KEY", $inter_key, -1, $sign, self::ENCRYPT_INTER);
        }
        
        $this->exchage_data = [
            "local_private" => $inter_key['private'],
            "remote_pub" => $dsaPubKey,
        ];
        $this->ecc_exchange();
        
        // kd($dsaPubKey);
    }
    
    public function ecc_exchange()
    {
        static $keys = [];
        if(empty($keys)) {
            if(!empty($this->exchage_data['remote_pub'])) {
                $keys['remote_pub'] = $this->exchage_data['remote_pub'];
                $keys['local_private'] = $this->exchage_data['local_private'];
                $this->exchage_data = [];
                return true;
            } else {
                return false;
            }
        }
        $this->exchage_data = [
            'privateKey' => $keys['local_private'],
            "publicKey" => $keys['local_private'],
            'shared' => 0
        ];
        $encryptKey = $this->ECDSA_exchange();
        
        return $encryptKey;
        
        //echo "Shared secret: " . $shared . PHP_EOL;
        //$this->ECDSA_exchange(Config::get("local.keys.user.private"), Config::get("local.keys.app.public"), $shared);
    }
    
    private $exchage_data = [];
    
    public function ECDSA_exchange()
    {
        // ECDSA domain is defined by curve/generator/hash algorithm,
        // which a verifier must be aware of.
        
        $privateKey = $publicKey = $shared = false;
        extract($this->exchage_data);
        
        $adapter = EccFactory::getAdapter();
        $generator = EccFactory::getNistCurves()->generator384();
        $useDerandomizedSignatures = true;
        
        
        $pemPriv = new PemPrivateKeySerializer(new DerPrivateKeySerializer());
        $pemPub = new PemPublicKeySerializer(new DerPublicKeySerializer());
        
        # These .pem and .key are for different keys
        $alicePriv = $pemPriv->parse($privateKey);
        $bobPub = $pemPub->parse($publicKey);
        if(empty($shared)) {
            $exchange = $alicePriv->createExchange($bobPub);
            $shared = $exchange->calculateSharedKey();
            
        } else {
            //echo "Shared secret: " . $shared . PHP_EOL;
            
            $shared = gmp_init($shared, 36);
        }
        
        
        # The shared key is never used directly, but used with a key derivation function (KDF)
        $kdf = function(GmpMathInterface $math, \GMP $sharedSecret) {
            $binary = $math->intToString($sharedSecret);
            $hash = hash('sha256', $binary, true);
            return $hash;
        };
        
        //$key = $kdf($adapter, $shared);
        
        $this->exchage_data = ['shared' => gmp_strval($shared, 36)];
        
        
        return unpack("H*", $kdf($adapter, $shared))[1];
        
        //echo "Encryption key: " . unpack("H*", $kdf($adapter, $shared))[1] . PHP_EOL;
        
        # This key can now be used to encrypt/decrypt messages with the other person
    }
    
    
    private $verisign_data = [];
    
    public function verisign($sign = null, $document = null, $keytype = self::ENCRYPT_INTER)
    {
        static $eccKeys = [];
        if(is_null($document)) {
            foreach($this->verisign_data as $k => $v) {
                if(isset($this->verisign_data[$k]) && empty($eccKeys[$k])) {
                    $eccKeys[$k] = $this->verisign_data[$k];
                }
            }
            $this->verisign_data = [];
            return true;
        }
        
        if(isset($eccKeys[$keytype])) {
            $publicKey = $eccKeys[$keytype];
        } else {
            $this->trigger_error('public key not exists: ' . $keytype, E_USER_WARNING);
            return false;
        }
        
        $adapter = EccFactory::getAdapter();
        $generator = EccFactory::getNistCurves()->generator384();
        $algorithm = 'sha256';
        $sigData = base64_decode($sign);
        
        // Parse signature
        $sigSerializer = new DerSignatureSerializer();
        $sig = $sigSerializer->parse($sigData);
        
        // Parse public key
        $derSerializer = new DerPublicKeySerializer($adapter);
        $pemSerializer = new PemPublicKeySerializer($derSerializer);
        $key = $pemSerializer->parse($publicKey);
        
        $signer = new Signer($adapter);
        $hash = $signer->hashData($generator, $algorithm, $document);
        $check = $signer->verify($key, $sig, $hash);
        return $check;
    }
    
    private $ecc_sign = [];
    
    /**
     * ECDSA-SHA256签名
     * @param string $data 需要签名的数据
     * @param $privateKey
     * @return string 返回签名 失败时返回false
     */
    private function sign()
    {
        list($document, $privateKey) = $this->ecc_sign;
        $this->ecc_sign = [];
        
        $adapter = EccFactory::getAdapter();
        $generator = EccFactory::getNistCurves()->generator256();
        $useDerandomizedSignatures = true;
        $algorithm = 'sha256';
        
        ## You'll be restoring from a key, as opposed to generating one.
        $pemSerializer = new PemPrivateKeySerializer(new DerPrivateKeySerializer($adapter));
        $key = $pemSerializer->parse($privateKey);
        
        
        $signer = new Signer($adapter);
        $hash = $signer->hashData($generator, $algorithm, $document);
        
        # Derandomized signatures are not necessary, but can reduce
        # the attack surface for a private key that is to be used often.
        if($useDerandomizedSignatures) {
            $random = \Mdanter\Ecc\Random\RandomGeneratorFactory::getHmacRandomGenerator($key, $hash, $algorithm);
        } else {
            $random = \Mdanter\Ecc\Random\RandomGeneratorFactory::getRandomGenerator();
        }
        
        $randomK = $random->generate($generator->getOrder());
        $signature = $signer->sign($key, $hash, $randomK);
        
        $serializer = new DerSignatureSerializer();
        $serializedSig = $serializer->serialize($signature);
        return base64_encode($serializedSig);
    }
    
    private function create()
    {
        $this->_rs = fopen($this->_file,
            'wb+') or $this->trigger_error('Can\'t open the cachefile: ' . realpath($this->_file), E_USER_ERROR);;
        if(!is_writable($this->_file) || !is_readable($this->_file)) {
            chmod($this->_file, 0666);
        }
        fseek($this->_rs, 0);
        fputs($this->_rs, '<' . '?php exit()?' . '>');
        return $this->_format();
    }
    
    private function _puts($offset, $data)
    {
        if($offset < $this->max_size * 1.5) {
            $this->_seek($offset);
            return fputs($this->_rs, $data);
        } else {
            $this->trigger_error('Offset over quota:' . $offset, E_USER_ERROR);
        }
    }
    
    private function _seek($offset)
    {
        return fseek($this->_rs, $offset);
    }
    
    function clean()
    {
        //$files = App::singleton('base_filesystem');
        File::delete($this->_file);
        // 重新初始化
        $this->workat($this->_file);
    }
    
    public function fetch($key)
    {
        $data = null;
        $this->internalFetch($key, $value);
        return $value['content'];
    }
    
    private function internalFetch($key, &$value, $lock = true)
    {
        // If the file doesn't exists, we obviously can't return the cache so we will
        // just return null. Otherwise, we'll get the contents of the file and get
        // the expiration UNIX timestamps from the start of the file's contents.
        try {
            if(false === $this->_fetch($key, $data, $lock)) {
                throw new Exception('Secached fetch fail.');
            }
            
            $expire = $data['expiration'];
            //$content = $data['content'];
        } catch(Exception $e) {
            
            $value = ['content' => null, 'expiration' => null];
            return false;
        }
        
        
        // If the current time is greater than expiration timestamps we will delete
        // the file and return null. This helps clean up the old files and keeps
        // this directory much cleaner for us as old files aren't hanging out.
        if($expire > 0 && time() >= $expire && $data['secret'] == self::ENCRYPT_NONE) {
            $this->delete($key);
            $value = ['content' => null, 'expiration' => null];
            return false;
        }
        
        //$time = ceil(($expire - time()) / 60);
        $value = $data;
        return true;
    }
    
    
    private function _fetch($key, &$return, $isLock = true)
    {
        if($isLock) {
            if($this->lock(false)) {
                $locked = true;
            } else {
                $locked = false;
            }
            
        } else {
            $locked = false;
        }
        
        if($this->search($key, $offset)) {
            $info = $this->_get_node($offset);
            $schema_id = $this->_get_size_schema_id($info['size']);
            if($schema_id === false) {
                if($locked) {
                    $this->unlock();
                }
                return false;
            }
            
            $this->_seek($info['data']);
            $data = fread($this->_rs, $info['size']);
            //$return = unserialize($data);
            $return = $this->encrypt($data, true);
            
            if($return === false) {
                if($locked) {
                    $this->unlock();
                }
                return false;
            }
            
            if($locked) {
                $this->_lru_push($schema_id, $info['offset']);
                $this->_set_schema($schema_id, 'hits', $this->_get_schema($schema_id, 'hits') + 1);
                return $this->unlock();
            } else {
                return true;
            }
        } else {
            if($locked) {
                $this->unlock();
            }
            return false;
        }
    }
    
    /**
     * lock
     * 如果flock不管用，请继承本类，并重载此方法
     *
     * @param mixed $is_block 是否阻塞
     * @return bool
     * @access public
     */
    private function lock($is_block)//, $whatever = false
    {
        ignore_user_abort(true);
        return flock($this->_rs, $is_block ? LOCK_EX : LOCK_EX + LOCK_NB);
    }
    
    /**
     * unlock
     * 如果flock不管用，请继承本类，并重载此方法
     *
     * @access public
     * @return bool
     */
    private function unlock()
    {
        ignore_user_abort(false);
        return flock($this->_rs, LOCK_UN);
    }
    
    public function delete($key, $pos = false, $sign = false)
    {
        if(empty($pos) && $this->_fetch($key, $default)) {
            if($default['secret'] != self::ENCRYPT_NONE) {
                if(!$this->verisign($sign, $key, $default['secret'])) {
                    $this->trigger_error('delete action: sign error:' . $key);
                    return false;
                }
            }
        }
        
        if($this->lock(true)) {
            if($pos || $this->search($key, $pos)) {
                if($info = $this->_get_node($pos)) {
                    //删除data区域
                    if($info['prev']) {
                        $this->_set_node($info['prev'], 'next', $info['next']);
                        $this->_set_node($info['next'], 'prev', $info['prev']);
                    } else { //改入口位置
                        $this->_set_node($info['next'], 'prev', 0);
                        $this->_set_node_root($key, $info['next']);
                    }
                    $this->_free_dspace($info['size'], $info['data']);
                    $this->_lru_delete($info);
                    $this->_free_node($pos);
                    return $info['prev'];
                }
            }
            return false;
        } else {
            $this->trigger_error("Couldn't lock the file !" . $this->_file, E_USER_WARNING);
            return false;
        }
        
        
    }
    
    const ENCRYPT_NONE = 0;
    const ENCRYPT_INTER = 1;
    const ENCRYPT_USER = 2;
    const ENCRYPT_APP = 3;
    const ENCRYPT_REMOTE = 4;
    const ENCRYPT_ADDON = 5;
    
    /**
     * @param $key
     * @param $value
     * @param $minutes
     * @param string $sign
     * @param int $secret
     * @return bool
     */
    public function store($key, $value, $minutes, $sign = '', $secret = self::ENCRYPT_NONE)
    {
        $data = [
            'expiration' => $this->expiration($minutes),
            'content' => $value,
            "sign" => $sign,
            "encrypt" => $secret,
        ];
        
        if($this->lock(true)) {
            $result = $this->storePayload($key, $data);
            //save data
            $this->unlock();
            return $result;
        } else {
            $this->trigger_error("Couldn't lock the file !" . $this->_file, E_USER_WARNING);
            return false;
        }
        return true;
    }
    
    private function storePayload($key, $value)
    {
        $secret = $sign = $content = false;
        extract($value);
        
        if($secret != self::ENCRYPT_NONE) {
            if($this->verisign($sign, serialize($content), $secret) == false) {
                $this->unlock();
                $this->trigger_error("sign error:" . $key, E_USER_WARNING);
                return false;
            }
        }
        
        if($this->internalFetch($key, $default)) {
            if($default['secret'] != self::ENCRYPT_NONE && $default['secret'] != $secret) {
                $this->trigger_error("secret level must be same: " . $key, E_USER_WARNING);
                return false;
            }
        }
        
        //$data = serialize($value);
        $data = $this->encrypt($value);
        $size = strlen($data);
        
        //get list_idx
        $has_key = $this->search($key, $list_idx_offset);
        //$key = $this->checkKey($key);
        
        $schema_id = $this->_get_size_schema_id($size);
        if($schema_id === false) {
            $this->unlock();
            return false;
        }
        if($has_key) {
            $hdseq = $list_idx_offset;
            
            $info = $this->_get_node($hdseq);
            if($schema_id == $this->_get_size_schema_id($info['size'])) {
                $dataoffset = $info['data'];
            } else {
                //破掉原有lru
                $this->_lru_delete($info);
                if(!($dataoffset = $this->_dalloc($schema_id))) {
                    $this->unlock();
                    return false;
                }
                $this->_free_dspace($info['size'], $info['data']);
                $this->_set_node($hdseq, 'lru_left', 0);
                $this->_set_node($hdseq, 'lru_right', 0);
            }
            
            $this->_set_node($hdseq, 'size', $size);
            $this->_set_node($hdseq, 'data', $dataoffset);
        } else {
            
            if(!($dataoffset = $this->_dalloc($schema_id))) {
                $this->unlock();
                return false;
            }
            $hdseq = $this->_alloc_idx(array(
                'next' => 0,
                'prev' => $list_idx_offset,
                'data' => $dataoffset,
                'size' => $size,
                'lru_right' => 0,
                'lru_left' => 0,
                'key' => $key,
            ));
            
            if($list_idx_offset > 0) {
                $this->_set_node($list_idx_offset, 'next', $hdseq);
            } else {
                $this->_set_node_root($key, $hdseq);
            }
        }
        
        if($dataoffset > $this->max_size) {
            $this->unlock();
            $this->trigger_error('alloc datasize:' . $dataoffset, E_USER_WARNING);
            return false;
        }
        $this->_puts($dataoffset, $data);
        
        $this->_set_schema($schema_id, 'miss', $this->_get_schema($schema_id, 'miss') + 1);
        
        $this->_lru_push($schema_id, $hdseq);
        
        return true;
        
    }
    
    public function increment($key, $value = 1, $initial = 0, $minutes)
    {
        return $this->decrement($key, -$value, $initial, $minutes);
    }
    
    public function decrement($key, $value = 1, $initial = 0, $minutes)
    {
        if($this->lock(true)) {
            $sign = null;
            if(is_array($key)) {
                list($key, $sign) = $key;
            }
            
            if($this->internalFetch($key, $origData, false)) {
                if($origData['secret'] != self::ENCRYPT_NONE && !$this->verisign($sign, $key, $origData['secret'])) {
                    $this->trigger_error('sign error:' . $key);
                    return false;
                }
            }
            $data = $origData;
            if($origData['content'] === null) {
                $data ['content'] = $initial;
                $data['expiration'] = $this->expiration($minutes);
            }
            
            //todo: 增加 time 逻辑
            $data['content'] = $data['content'] - $value;
            
            if($data['content'] < 0) {
                $data['content'] = 0;
            }
            
            //var_dump($data);
            $this->storePayload($key, $data);
            //save data
            $this->unlock();
            return $data['content'];
            //return true;
        } else {
            $this->trigger_error("Couldn't lock the file !" . $this->_file, E_USER_WARNING);
            return false;
        }
    }
    
    private function checkKey(&$key)
    {
        if(preg_replace('/[^0-9A-Fa-f]/', '', $key) != $key || strlen($key) != 32) {
            $key = $name = strtolower($key);
            $pre = '';
            if(strpos($key, ':')) {
                list($pre, $name) = explode(':', $key, 2);
            }
            if(!in_array($pre, $this->namespace)) {
                $pre = 'user';
                $name = $key;
            }
            $key = md5(hash_pbkdf2('sha256', $pre . ":" . $name, $this->ver, 1230));
        }
        return $key;
    }
    
    /**
     * search
     * 查找指定的key
     * 如果找到节点则$pos=节点本身 返回true
     * 否则 $pos=树的末端 返回false
     *
     * @param mixed $key
     * @param $pos
     * @return bool
     * @access public
     */
    private function search(&$key, &$pos)
    {
        $key = $this->checkKey($key);
        return $this->_get_pos_by_key($this->_get_node_root($key), $key, $pos);
    }
    
    private function _get_size_schema_id($size)
    {
        foreach($this->_block_size_list as $k => $block_size) {
            if($size <= $block_size) {
                return $k;
            }
        }
        return false;
    }
    
    private function _parse_str_size($str_size, $default)
    {
        if(preg_match('/^([0-9]+)\s*([gmk]|)$/i', $str_size, $match)) {
            switch(strtolower($match[2])) {
                case 'g':
                    if($match[1] > 1) {
                        $this->trigger_error('Max cache size 1G', E_USER_ERROR);
                    }
                    $size = $match[1] << 30;
                    break;
                case 'm':
                    $size = $match[1] << 20;
                    break;
                case 'k':
                    $size = $match[1] << 10;
                    break;
                default:
                    $size = $match[1];
            }
            if($size <= 0) {
                $this->trigger_error('Error cache size ' . $this->max_size, E_USER_ERROR);
                return false;
            } elseif($size < 10485760) {
                return 10485760;
            } else {
                return $size;
            }
        } else {
            return $default;
        }
    }
    
    
    private function _format($truncate = false)
    {
        if($this->lock(true)) {//, true
            
            if($truncate) {
                $this->_seek(0);
                ftruncate($this->_rs, $this->idx_node_base);
            }
            
            if(function_exists('mcrypt_create_iv')) {
                $rootKey = @mcrypt_create_iv(12, MCRYPT_DEV_RANDOM);
            } else {
                throw new Exception("mcrypt extension not installed");
            }
            
            $this->max_size = $this->_parse_str_size($this->secache_size, 15728640); //default:15m
            if(version_compare(phpversion(), '5.5', 'gt')) {
                $this->_puts($this->header_padding,
                    pack('V1Z*', $this->max_size, substr(md5($this->ver), 0, 4) . $rootKey));
            } else {
                $this->_puts($this->header_padding,
                    pack('V1a*', $this->max_size, substr(md5($this->ver), 0, 4) . $rootKey));
            }
            
            $this->root_key = $rootKey;
            $this->encrypt(null);
            
            ksort($this->_bsize_list);
            $ds_offset = $this->data_core_pos;
            $i = 0;
            foreach($this->_bsize_list as $size => $count) {
                
                //将预分配的空间注册到free链表里
                $count *= min(3, floor($this->max_size / 10485760));
                $next_free_node = 0;
                for($j = 0; $j < $count; $j++) {
                    $this->_puts($ds_offset, pack('V', $next_free_node));
                    $next_free_node = $ds_offset;
                    $ds_offset += intval($size);
                }
                
                $code = pack(str_repeat('V1', count($this->schema_struct)), $size, $next_free_node, 0, 0, 0, 0);
                
                $this->_puts(60 + $i * $this->schema_item_size, $code);
                $i++;
            }
            $this->_set_dcur_pos($ds_offset);
            
            $this->_puts($this->idx_core_pos, str_repeat("\0", 262144));
            $this->_puts($this->idx_seq_pos, pack('V', 1));
            $this->unlock();
            return true;
        } else {
            $this->trigger_error("Couldn't lock the file !" . $this->_file, E_USER_ERROR);
            return false;
        }
    }
    
    private function _get_node_root($key)
    {
        $this->_seek(hexdec(substr($key, 0, 4)) * 4 + $this->idx_core_pos);
        $a = fread($this->_rs, 4);
        list(, $offset) = unpack('V', $a);
        return $offset;
    }
    
    private function _set_node_root($key, $value)
    {
        return $this->_puts(hexdec(substr($key, 0, 4)) * 4 + $this->idx_core_pos, pack('V', $value));
    }
    
    private function _set_node($pos, $key, $value)
    {
        
        if(!$pos) {
            return false;
        }
        
        if(isset($this->_node_struct[$key])) {
            return $this->_puts($pos * $this->idx_node_size + $this->idx_node_base + $this->_node_struct[$key][0],
                pack($this->_node_struct[$key][1], $value));
        } else {
            return false;
        }
    }
    
    private function _get_pos_by_key($offset, $key, &$pos)
    {
        if(!$offset) {
            $pos = 0;
            return false;
        }
        
        $info = $this->_get_node($offset);
        
        if($info['key'] == $key) {
            $pos = $info['offset'];
            return true;
        } elseif($info['next'] && $info['next'] != $offset) {
            return $this->_get_pos_by_key($info['next'], $key, $pos);
        } else {
            $pos = $offset;
            return false;
        }
    }
    
    private function _lru_delete($info)
    {
        
        if($info['lru_right']) {
            $this->_set_node($info['lru_right'], 'lru_left', $info['lru_left']);
        } else {
            $this->_set_schema($this->_get_size_schema_id($info['size']), 'lru_tail', $info['lru_left']);
        }
        
        if($info['lru_left']) {
            $this->_set_node($info['lru_left'], 'lru_right', $info['lru_right']);
        } else {
            $this->_set_schema($this->_get_size_schema_id($info['size']), 'lru_head', $info['lru_right']);
        }
        
        return true;
    }
    
    private function _lru_push($schema_id, $offset)
    {
        $lru_head = $this->_get_schema($schema_id, 'lru_head');
        $lru_tail = $this->_get_schema($schema_id, 'lru_tail');
        
        if((!$offset) || ($lru_head == $offset)) {
            return;
        }
        
        $info = $this->_get_node($offset);
        
        $this->_set_node($info['lru_right'], 'lru_left', $info['lru_left']);
        $this->_set_node($info['lru_left'], 'lru_right', $info['lru_right']);
        
        $this->_set_node($offset, 'lru_right', $lru_head);
        $this->_set_node($offset, 'lru_left', 0);
        
        $this->_set_node($lru_head, 'lru_left', $offset);
        $this->_set_schema($schema_id, 'lru_head', $offset);
        
        if($lru_tail == 0) {
            $this->_set_schema($schema_id, 'lru_tail', $offset);
        } elseif($lru_tail == $offset && $info['lru_left']) {
            $this->_set_schema($schema_id, 'lru_tail', $info['lru_left']);
        }
        return true;
    }
    
    private function _get_node($offset)
    {
        $this->_seek($offset * $this->idx_node_size + $this->idx_node_base);
        $info = unpack('V1next/V1prev/V1data/V1size/V1lru_right/V1lru_left/H*key',
            fread($this->_rs, $this->idx_node_size));
        $info['offset'] = $offset;
        return $info;
    }
    
    private function _lru_pop($schema_id)
    {
        if($node = $this->_get_schema($schema_id, 'lru_tail')) {
            $info = $this->_get_node($node);
            if(!$info['data']) {
                return false;
            }
            $this->delete($info['key'], $info['offset']);
            if(!$this->_get_schema($schema_id, 'free')) {
                $this->trigger_error('pop lru,But nothing free...', E_USER_ERROR);
            }
            return $info;
        } else {
            return false;
        }
    }
    
    private function _dalloc($schema_id, $lru_freed = false)
    {
        
        if($free = $this->_get_schema($schema_id, 'free')) { //如果lru里有链表
            $this->_seek($free);
            list(, $next) = unpack('V', fread($this->_rs, 4));
            $this->_set_schema($schema_id, 'free', $next);
            return $free;
        } elseif($lru_freed) {
            $this->trigger_error('Bat lru poped freesize', E_USER_ERROR);
            return false;
        } else {
            $ds_offset = $this->_get_dcur_pos();
            $size = $this->_get_schema($schema_id, 'size');
            
            if($size + $ds_offset > $this->max_size) {
                if($info = $this->_lru_pop($schema_id)) {
                    return $this->_dalloc($schema_id, $info);
                } else {
                    $this->trigger_error('Can\'t alloc dataspace', E_USER_ERROR);
                    return false;
                }
            } else {
                $this->_set_dcur_pos($ds_offset + $size);
                return $ds_offset;
            }
        }
    }
    
    private function _get_dcur_pos()
    {
        $this->_seek($this->dfile_cur_pos);
        list(, $ds_offset) = unpack('V', fread($this->_rs, 4));
        return $ds_offset;
    }
    
    private function _set_dcur_pos($pos)
    {
        return $this->_puts($this->dfile_cur_pos, pack('V', $pos));
    }
    
    private function _free_dspace($size, $pos)
    {
        
        if($pos > $this->max_size) {
            $this->trigger_error('free dspace over quota:' . $pos, E_USER_ERROR);
            return false;
        }
        
        $schema_id = $this->_get_size_schema_id($size);
        if($free = $this->_get_schema($schema_id, 'free')) {
            $this->_puts($free, pack('V1', $pos));
        } else {
            $this->_set_schema($schema_id, 'free', $pos);
        }
        $this->_puts($pos, pack('V1', 0));
    }
    
    private function _dfollow($pos, &$c)
    {
        $c++;
        $this->_seek($pos);
        list(, $next) = unpack('V1', fread($this->_rs, 4));
        if($next) {
            return $this->_dfollow($next, $c);
        } else {
            return $pos;
        }
    }
    
    private function _free_node($pos)
    {
        $this->_seek($this->idx_free_pos);
        list(, $prev_free_node) = unpack('V', fread($this->_rs, 4));
        $this->_puts($pos * $this->idx_node_size + $this->idx_node_base,
            pack('V', $prev_free_node) . str_repeat("\0", $this->idx_node_size - 4));
        return $this->_puts($this->idx_free_pos, pack('V', $pos));
    }
    
    private function _alloc_idx($data)
    {
        $this->_seek($this->idx_free_pos);
        list(, $list_pos) = unpack('V', fread($this->_rs, 4));
        if($list_pos) {
            
            $this->_seek($list_pos * $this->idx_node_size + $this->idx_node_base);
            list(, $prev_free_node) = unpack('V', fread($this->_rs, 4));
            $this->_puts($this->idx_free_pos, pack('V', $prev_free_node));
            
        } else {
            $this->_seek($this->idx_seq_pos);
            list(, $list_pos) = unpack('V', fread($this->_rs, 4));
            $this->_puts($this->idx_seq_pos, pack('V', $list_pos + 1));
        }
        return $this->_create_node($list_pos, $data);
    }
    
    private function _create_node($pos, $data)
    {
        $this->_puts($pos * $this->idx_node_size + $this->idx_node_base
            , pack('V1V1V1V1V1V1H*', $data['next'], $data['prev'], $data['data'], $data['size'], $data['lru_right'],
                $data['lru_left'], $data['key']));
        return $pos;
    }
    
    private function _set_schema($schema_id, $key, $value)
    {
        $info = array_flip($this->schema_struct);
        return $this->_puts(60 + $schema_id * $this->schema_item_size + $info[$key] * 4, pack('V', $value));
    }
    
    private function _get_schema($id, $key)
    {
        $info = array_flip($this->schema_struct);
        
        $this->_seek(60 + $id * $this->schema_item_size);
        unpack('V1' . implode('/V1', $this->schema_struct), fread($this->_rs, $this->schema_item_size));
        
        $this->_seek(60 + $id * $this->schema_item_size + $info[$key] * 4);
        list(, $value) = unpack('V', fread($this->_rs, 4));
        return $value;
    }
    
    private function _all_schemas()
    {
        $schema = array();
        for($i = 0; $i < 16; $i++) {
            $this->_seek(60 + $i * $this->schema_item_size);
            $info = unpack('V1' . implode('/V1', $this->schema_struct), fread($this->_rs, $this->schema_item_size));
            if($info['size']) {
                $info['id'] = $i;
                $schema[$i] = $info;
            } else {
                return $schema;
            }
        }
    }
    
    private function schemaStatus()
    {
        $return = array();
        foreach($this->_all_schemas() as $k => $schemaItem) {
            if($schemaItem['free']) {
                $this->_dfollow($schemaItem['free'], $schemaItem['freecount']);
            }
            $return[] = $schemaItem;
        }
        return $return;
    }
    
    public function status(&$curBytes, &$totalBytes)
    {
        $totalBytes = $curBytes = 0;
        $hits = $miss = 0;
        
        $schemaStatus = $this->schemaStatus();
        $totalBytes = $this->max_size;
        $freeBytes = $this->max_size - $this->_get_dcur_pos();
        
        foreach($schemaStatus as $schema) {
            $freeBytes += $schema['freecount'] * $schema['size'];
            $miss += $schema['miss'];
            $hits += $schema['hits'];
        }
        $curBytes = $totalBytes - $freeBytes;
        
        $return[] = array('name' => app::get('base')->_('缓存命中'), 'value' => $hits);
        $return[] = array('name' => app::get('base')->_('缓存未命中'), 'value' => $miss);
        return $return;
    }
    
    function trigger_error($errstr, $errno = E_USER_WARNING)
    {
        $this->unlock();
        
        if($errno == E_USER_ERROR) {
            if(!$this->_in_fatal_error) {
                $this->_in_fatal_error = true;
                //$this->_format(true);
            }
            if(function_exists('debug_print_backtrace')) {
                echo '<h1>' . $errstr . '</h1><hr />';
                echo '<pre>';
                debug_print_backtrace();
                echo '</pre>';
                exit;
            } else {
                trigger_error($errstr, $errno);
            }
        } else {
            trigger_error($errstr, $errno);
        }
    }
    
    /**
     * Get the expiration time based on the given minutes.
     *
     * @param  int $minutes
     * @return int
     */
    protected function expiration($minutes)
    {
        if($minutes === 0) {
            return -1;// 9999999999;
        }
        
        return time() + ($minutes * 60);
    }
    
}
