<?php namespace App\System\Http\Controllers;

use Homeshop\Modules\Routing\Controller;

class SystemController extends Controller {
	
	public function index()
	{
		return view('system::index');
	}
	
}