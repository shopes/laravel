const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    //mix.sass('app.scss').webpack('app.js');
    mix.sass(['app.scss'],'public/assets/css');

    // 将 Vue.js 转为普通 js 文件
    mix.browserify('entries/hello.js', 'public/assets/js/hello.js');

    // 实时监听文件，不需要可以不用
    mix.browserSync({
        proxy: 'weipay.dev', // 你的本地域名，根据需要自行修改
        port: 3000,
        notify: false,
        watchTask: true,
        open: 'external',
        host: 'weipay.dev', // 你的本地域名，根据需要自行修改
    });
});
