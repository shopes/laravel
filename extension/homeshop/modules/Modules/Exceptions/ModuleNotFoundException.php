<?php

namespace Homeshop\Modules\Exceptions;

class ModuleNotFoundException extends \Exception
{
}
