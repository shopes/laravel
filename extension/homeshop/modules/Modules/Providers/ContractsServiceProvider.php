<?php

namespace Homeshop\Modules\Providers;

use Illuminate\Support\ServiceProvider;

class ContractsServiceProvider extends ServiceProvider
{
    /**
     * Register some binding.
     */
    public function register()
    {
        $this->app->bind(
            'Homeshop\Modules\Contracts\RepositoryInterface',
            'Homeshop\Modules\Repository'
        );
    }
}
