<?php

namespace Homeshop\Modules\Process;

use Homeshop\Modules\Contracts\RunableInterface;
use Homeshop\Modules\Repository;

class Runner implements RunableInterface
{
    /**
     * The module instance.
     *
     * @var \Homeshop\Modules\Repository
     */
    protected $module;

    /**
     * The constructor.
     *
     * @param \Homeshop\Modules\Repository $module
     */
    public function __construct(Repository $module)
    {
        $this->module = $module;
    }

    /**
     * Run the given command.
     *
     * @param string $command
     */
    public function run($command)
    {
        passthru($command);
    }
}
