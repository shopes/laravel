<?php
/**
 * Created by IntelliJ IDEA.
 * User: Mo
 * Date: 2017/2/13
 * Time: 下午7:36
 */

define('APP_DIR', realpath(dirname(__DIR__) . '/app'));
define('WP_ROOT', realpath(dirname(__DIR__) . '/public/app/topic'));
define('LARAVEL_ROOT', dirname(__DIR__));


function __($text, $domain = 'default', $locale = null)
{
    if (class_exists('translator')) {
        if (is_array($domain) || !is_null($locale)) {
            return translate($text, app('translator')->getFromJson($text, $domain, $locale), 'default');
        } else {
            return app('translator')->getFromJson(translate($text, $domain), [], null);
        }
    } else {
        return translate($text, $domain);
    }
}