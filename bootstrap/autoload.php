<?php

use App\Helpers\ClassLoader;



define('LARAVEL_START', microtime(true));

require __DIR__.'/define.php';

/*
|--------------------------------------------------------------------------
| Register The Composer Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader
| for our application. We just need to utilize it! We'll require it
| into the script here so that we do not have to worry about the
| loading of any our classes "manually". Feels great to relax.
|
*/

$autoloader = require __DIR__ . '/../vendor/autoload.php';

spl_autoload_register(function($class) use ($autoloader) {
    static $_loader;
    empty($_loader) && $_loader = $autoloader;
    $className = ucwords($class, '\\');
    if($class !== $className) {
        if(!class_exists($className)) {
            $_loader->loadClass($className);
        }
        if(!class_exists($className)) {
            Illuminate\Foundation\AliasLoader::getInstance()->load($className);
        }
        //        if(class_exists($className,false)){
        //            class_alias($className, $class);
        //        }
        //k($className,class_exists($className)) ;
    }
});

ClassLoader::register();

/*
|--------------------------------------------------------------------------
| Include The Compiled Class File
|--------------------------------------------------------------------------
|
| To dramatically increase your application's performance, you may use a
| compiled class file which contains all of the classes commonly used
| by a request. The Artisan "optimize" is used to create this file.
|
*/

$compiledPath = __DIR__ . '/cache/compiled.php';

if(file_exists($compiledPath)) {
    require $compiledPath;
}
