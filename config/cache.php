<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Cache Store
    |--------------------------------------------------------------------------
    |
    | This option controls the default cache connection that gets used while
    | using this caching library. This connection is used when another is
    | not explicitly specified when executing a given caching function.
    |
    | Supported: "apc", "array", "database", "file", "memcached", "redis"
    |
    */

    'default' => env('CACHE_DRIVER', 'file'),

    /*
    |--------------------------------------------------------------------------
    | Cache Stores
    |--------------------------------------------------------------------------
    |
    | Here you may define all of the cache "stores" for your application as
    | well as their drivers. You may even define multiple stores for the
    | same cache driver to group types of items stored in your caches.
    |
    */

    'stores' => [

        'apc' => [
            'driver' => 'apc',
        ],

        'array' => [
            'driver' => 'array',
        ],

        'database' => [
            'driver' => 'database',
            'table' => 'cache',
            'connection' => null,
        ],

        'file' => [
            'driver' => 'file',
            'path' => storage_path('framework/cache'),
        ],

        'memcached' => [
            'driver' => 'memcached',
            'persistent_id' => env('MEMCACHED_PERSISTENT_ID'),
            'sasl' => [
                env('MEMCACHED_USERNAME'),
                env('MEMCACHED_PASSWORD'),
            ],
            'options' => [
                // Memcached::OPT_CONNECT_TIMEOUT  => 2000,
            ],
            'servers' => [
                [
                    'host' => env('MEMCACHED_HOST', '127.0.0.1'),
                    'port' => env('MEMCACHED_PORT', 11211),
                    'weight' => 100,
                ],
            ],
        ],

        'redis' => [
            'driver' => 'redis',
            'connection' => 'default',
        ],

    ],
    
    /*
|--------------------------------------------------------------------------
| Cache Resources
|--------------------------------------------------------------------------
|
| 这里定义缓存的所有资源,在stores里需要制定对应的资源
| `resource`
| `driver`分为两类,
| local(本地): 数据存储在本地Web服务器上, apc secache.
| remote(中心化): memcached
| `driver` 默认配置里分为三类
|
|
*/
    'resources' => [
        // 此条勿要删除, 系统默认使用
        'null'=> [
            'driver' => 'null'
        ],
        
        // driver: `local`类型
        'local' => [
            'driver' => 'secache',
            'file' => 'local',
            'size' => '10m',
        ],
        
        // driver: `remote`类型
        'default' => [
            'driver' => 'secache',
            'file' => 'common',
            'size' => '10m',
        ],
        
        'remote-session' => [
            'driver' => 'secache',
            'file' => 'session',
            'size' => '1g',
        ],
        
        'apc' => [
            'driver' => 'apc'
        ],
        
        'memcached' => [
            'driver' => 'memcached',
            'servers' => [
                ['host' => '127.0.0.1', 'port' => 11211, 'weight' => 100],
            ],
        ],
    ],
    
    
    /*
    |--------------------------------------------------------------------------
    | Cache Key Prefix
    |--------------------------------------------------------------------------
    |
    | When utilizing a RAM based store such as APC or Memcached, there might
    | be other applications utilizing the same cache. So, we'll specify a
    | value to get prefixed to all our keys so we can avoid collisions.
    |
    */

    'prefix' => 'laravel',

];
