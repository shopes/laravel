/*
Theme Name: Idoneita
Author: Stefan C.
Theme URI: https://www.pwtthemes.com/theme/idoneita-free-responsive-wordpress-theme
Author URI: http://www.stefanciobanu.com
Description: Idoneita is a great looking modern free responsive WordPress theme that is perfect for a fitness club, as well as other various business. Idoneita designed theme comes with easy to manage theme options in customizer.
Version: 1.0.6
Tags: entertainment, one-column, two-columns, right-sidebar, grid-layout, footer-widgets, custom-menu, custom-background, editor-style, featured-images, full-width-template, theme-options, threaded-comments, translation-ready
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Text Domain:  idoneita 
*/


== Copyright ==
Idoneita WordPress Theme, Copyright (C) 2016 Stefan C.
Idoneita WordPress Theme is licensed under the GPL 3.

Idoneita is built with the following resources: 

Owl Carousel - https://github.com/OwlFonk/OwlCarousel
License: MIT 
Copyright: owlgraphic, http://owlgraphic.com/owlcarousel/

HTML5 - https://github.com/aFarkas/html5shiv
License: MIT/GPL2 Licensed
Copyright: Alexander Farkas, https://github.com/aFarkas

Css3 mediaqueries  - https://code.google.com/archive/p/css3-mediaqueries-js/
License:  MIT License
Copyright: Google Code, https://code.google.com/archive/p/css3-mediaqueries-js/

Main JS, Custom Customize  - http://www.stefanciobanu.com
License: GPLv2 or later
Copyright: Stefan Ciobanu, http://www.stefanciobanu.com

Font Awesome - http://fortawesome.github.io/Font-Awesome/
License: SIL OFL 1.1
Copyright: Dave Gandy, http://fortawesome.github.io/Font-Awesome/license/

Open Sans - http://www.fontsquirrel.com/fonts/open-sans
License: Apache License v2.00
Copyright: Ascender Fonts, http://www.ascenderfonts.com

Images Owl Carousel (AjaxLoader.png, grabbing.png) - https://github.com/OwlFonk/OwlCarousel
License: MIT 
Copyright: owlgraphic, http://owlgraphic.com/owlcarousel/

Images (gray-pixel.png, overlay-blue.png, separe.png, texture.png) - http://www.pwtthemes.com/
License: GPLv2 or later
Copyright: Stefan Ciobanu, http://www.stefanciobanu.com

Images on the demo:  https://pixabay.com/en/stretching-muscles-runner-jogger-579122/
License: CC0 Public Domain
Copyright: skeeze, https://pixabay.com/en/users/skeeze-272447/

Images on the demo:  https://pixabay.com/en/running-runner-long-distance-573762/
License: CC0 Public Domain
Copyright: skeeze, https://pixabay.com/en/users/skeeze-272447/

== Installation ==

1. Upload the `idoneita` folder to the `/wp-content/themes/` directory
Activation and Use
1. Activate the Theme through the 'Themes' menu in WordPress
2. See Appearance -> Theme Options to change theme options
