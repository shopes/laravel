<?php
/**
 * Idoneita Theme Customizer
 *
 * @package Idoneita
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
 
function idoneita_customize_register( $wp_customize ) {
	
	// Update Your Settings
	
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	

    // Add General Sections

    $wp_customize->add_section('general',             array('title' => __('General', 'idoneita'),          'description' => '',                                         'priority' => 130,));
    $wp_customize->add_section('socialmedia',         array('title' => __('Social Media', 'idoneita'),     'description' => '',                                         'priority' => 180,));	

	// Add Panels

	$wp_customize->add_panel('slider',                array('title' => __('Slider', 'idoneita' ),          'description' => __( 'Slides Details', 'idoneita' ),           'priority' => 140));
    $wp_customize->add_panel('homepage',              array('title' => __('Home Page', 'idoneita'),        'description' => '',                                         'priority' => 160,));


	// Panels Slider
		
	$wp_customize->add_section('slide1',              array('title' => __('Slide 1', 'idoneita'),          'description' => '',             'panel' => 'slider',		  'priority' => 140,));
	$wp_customize->add_section('slide2',              array('title' => __('Slide 2', 'idoneita'),          'description' => '',             'panel' => 'slider',		  'priority' => 140,));
	
	// Panels Home Page

	$wp_customize->add_section('whyus',               array('title' => __('Why us', 'idoneita'),           'description' => '',             'panel' => 'homepage',	   'priority' => 140,));		


 	
	
    // Add Control General Settings
	
	$wp_customize->add_setting('pwt_info_box_title',array(
			'default'	=> '',
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_info_box_title',array(
			'label'	=> __('Info Box Title','idoneita'),
			'section'	=> 'general',
			'setting'	=> 'pwt_info_box_title'
	));		
	

	$wp_customize->add_setting('pwt_blog_page_title',array(
			'default'	=> __('Latest Blog','idoneita'),
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_blog_page_title',array(
			'label'	=> __('Blog Page Title','idoneita'),
			'section'	=> 'general',
			'setting'	=> 'pwt_blog_page_title'
	));		
	
	$wp_customize->add_setting('pwt_copyrights',array(
			'default'	=> '',
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_copyrights',array(
			'label'	=> __('Copyrights Text','idoneita'),
			'section'	=> 'general',
			'setting'	=> 'pwt_copyrights'
	));		
	
    // Add Control Slider
	
	$wp_customize->add_setting('pwt_slider_content1',array(
			'default'	=> __('0','idoneita'),
			'sanitize_callback'	=> 'idoneita_sanitize_integer'
			
	));
	
	$wp_customize->add_control('pwt_slider_content1',array(
			'label'	=> __('Slider Content 1','idoneita'),
			'section'	=> 'slide1',
			'setting'	=> 'pwt_slider_content1',
			'type' => 'dropdown-pages'
	));		

	$wp_customize->add_setting('pwt_slider_button_text_1',array(
			'default'	=> '',
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_slider_button_text_1',array(
			'label'	=> __('Slider Button Text','idoneita'),
			'section'	=> 'slide1',
			'setting'	=> 'pwt_slider_button_text_1'
	));			
	

	$wp_customize->add_setting('pwt_slider_content2',array(
			'default'	=> __('0','idoneita'),
			'sanitize_callback'	=> 'idoneita_sanitize_integer'
			
	));
	
	$wp_customize->add_control('pwt_slider_content2',array(
			'label'	=> __('Slider Content 2','idoneita'),
			'section'	=> 'slide2',
			'setting'	=> 'pwt_slider_content2',
			'type' => 'dropdown-pages'
	));		

	$wp_customize->add_setting('pwt_slider_button_text_2',array(
			'default'	=> '',
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_slider_button_text_2',array(
			'label'	=> __('Slider Button Text','idoneita'),
			'section'	=> 'slide2',
			'setting'	=> 'pwt_slider_button_text_2'
	));		
	
		
		

    // Add Control Home Page	
			

	$wp_customize->add_setting('pwt_whyus_icon_1',array(
			'default'	=> __('bookmark','idoneita'),
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_whyus_icon_1',array(
			'label'	=> __('Icon 1','idoneita'),
			'description' => __('Select a icon in this list <a target="_blank" href="http://fortawesome.github.io/Font-Awesome/icons/">http://fortawesome.github.io/Font-Awesome/icons/</a> and enter the code','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_icon_1'
	));			
			
	$wp_customize->add_setting('pwt_whyus_title_1',array(
			'default'	=> __('Why Us','idoneita'),
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_whyus_title_1',array(
			'label'	=> __('Title 1','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_title_1'
	));		

	$wp_customize->add_setting('pwt_whyus_content_1',array(
			'default'	=> '',
			'sanitize_callback'	=> 'esc_textarea'
	));
	
	$wp_customize->add_control('pwt_whyus_content_1',array(
	        'type' => 'textarea',
			'label'	=> __('Content 1','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_content_1'
	));	

	$wp_customize->add_setting('pwt_whyus_button_link_1',array(
			'default'	=> __('Read More','idoneita'),
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_whyus_button_link_1',array(
			'label'	=> __('Button Text 1','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_button_link_1'
	));	
	
	$wp_customize->add_setting('pwt_whyus_link_1',array(
			'default'	=> __('#','idoneita'),
			'sanitize_callback'	=> 'esc_url_raw'
	));
	
	$wp_customize->add_control('pwt_whyus_link_1',array(
			'label'	=> __('Link 1','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_link_1'
	));		
	
	$wp_customize->add_setting('pwt_whyus_icon_2',array(
			'default'	=> __('laptop','idoneita'),
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_whyus_icon_2',array(
			'label'	=> __('Icon 2','idoneita'),
			'description' => __('Select a icon in this list <a target="_blank" href="http://fortawesome.github.io/Font-Awesome/icons/">http://fortawesome.github.io/Font-Awesome/icons/</a> and enter the code','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_icon_2'
	));			
			
	$wp_customize->add_setting('pwt_whyus_title_2',array(
			'default'	=> __('Our Vision','idoneita'),
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_whyus_title_2',array(
			'label'	=> __('Title 2','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_title_2'
	));		

	$wp_customize->add_setting('pwt_whyus_content_2',array(
			'default'	=> '',
			'sanitize_callback'	=> 'esc_textarea'
	));
	
	$wp_customize->add_control('pwt_whyus_content_2',array(
	        'type' => 'textarea',
			'label'	=> __('Content 2','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_content_2'
	));		
	
	$wp_customize->add_setting('pwt_whyus_button_link_2',array(
			'default'	=> __('Read More','idoneita'),
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_whyus_button_link_2',array(
			'label'	=> __('Button Text 2','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_button_link_2'
	));	
	
	$wp_customize->add_setting('pwt_whyus_link_2',array(
			'default'	=> __('#','idoneita'),
			'sanitize_callback'	=> 'esc_url_raw'
	));
	
	$wp_customize->add_control('pwt_whyus_link_2',array(
			'label'	=> __('Link 2','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_link_2'
	));	

	$wp_customize->add_setting('pwt_whyus_icon_3',array(
			'default'	=> __('graduation-cap','idoneita'),
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_whyus_icon_3',array(
			'label'	=> __('Icon 3','idoneita'),
			'description' => __('Select a icon in this list <a target="_blank" href="http://fortawesome.github.io/Font-Awesome/icons/">http://fortawesome.github.io/Font-Awesome/icons/</a> and enter the code','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_icon_3'
	));			
			
	$wp_customize->add_setting('pwt_whyus_title_3',array(
			'default'	=> __('Our Location','idoneita'),
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_whyus_title_3',array(
			'label'	=> __('Title 3','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_title_3'
	));		

	$wp_customize->add_setting('pwt_whyus_content_3',array(
			'default'	=> '',
			'sanitize_callback'	=> 'esc_textarea'
	));
	
	$wp_customize->add_control('pwt_whyus_content_3',array(
	        'type' => 'textarea',
			'label'	=> __('Content 3','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_content_3'
	));	

	$wp_customize->add_setting('pwt_whyus_button_link_3',array(
			'default'	=> __('Read More','idoneita'),
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_whyus_button_link_3',array(
			'label'	=> __('Button Text 3','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_button_link_3'
	));		
	
	$wp_customize->add_setting('pwt_whyus_link_3',array(
			'default'	=> __('#','idoneita'),
			'sanitize_callback'	=> 'esc_url_raw'
	));
	
	$wp_customize->add_control('pwt_whyus_link_3',array(
			'label'	=> __('Link Button 3','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_link_3'
	));	

	$wp_customize->add_setting('pwt_whyus_icon_4',array(
			'default'	=> __('support','idoneita'),
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_whyus_icon_4',array(
			'label'	=> __('Icon 4','idoneita'),
			'description' => __('Select a icon in this list <a target="_blank" href="http://fortawesome.github.io/Font-Awesome/icons/">http://fortawesome.github.io/Font-Awesome/icons/</a> and enter the code','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_icon_4'
	));			
			
	$wp_customize->add_setting('pwt_whyus_title_4',array(
			'default'	=> __('Support','idoneita'),
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_whyus_title_4',array(
			'label'	=> __('Title 4','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_title_4'
	));		

	$wp_customize->add_setting('pwt_whyus_content_4',array(
			'default'	=> '',
			'sanitize_callback'	=> 'esc_textarea'
	));
	
	$wp_customize->add_control('pwt_whyus_content_4',array(
	        'type' => 'textarea',
			'label'	=> __('Content 4','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_content_4'
	));		
	
	$wp_customize->add_setting('pwt_whyus_button_link_4',array(
			'default'	=> __('Read More','idoneita'),
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_whyus_button_link_4',array(
			'label'	=> __('Button Text 4','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_button_link_4'
	));	
	
	$wp_customize->add_setting('pwt_whyus_link_4',array(
			'default'	=> __('#','idoneita'),
			'sanitize_callback'	=> 'esc_url_raw'
	));
	
	$wp_customize->add_control('pwt_whyus_link_4',array(
			'label'	=> __('Link Button 4','idoneita'),
			'section'	=> 'whyus',
			'setting'	=> 'pwt_whyus_link_4'
	));	

    // Social Media	
	

	$wp_customize->add_setting('pwt_social_media_code1',array(
			'default'	=> '',
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_social_media_code1',array(
			'label'	=> __('Social Media Code 1','idoneita'),
			'description' => __('Select a icon in this list <a target="_blank" href="http://fortawesome.github.io/Font-Awesome/icons/">http://fortawesome.github.io/Font-Awesome/icons/</a> and enter the code','idoneita'),
			'section'	=> 'socialmedia',
			'setting'	=> 'pwt_social_media_code1'
	));		
	
	$wp_customize->add_setting('pwt_social_media_link1',array(
			'default'	=> '',
			'sanitize_callback'	=> 'esc_url_raw'
	));
	
	$wp_customize->add_control('pwt_social_media_link1',array(
			'label'	=> __('Social Media Link 1','idoneita'),
			'section'	=> 'socialmedia',
			'setting'	=> 'pwt_social_media_link1'
	));		
	
	$wp_customize->add_setting('pwt_social_media_code2',array(
			'default'	=> '',
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_social_media_code2',array(
			'label'	=> __('Social Media Code 2','idoneita'),
			'description' => __('Select a icon in this list <a target="_blank" href="http://fortawesome.github.io/Font-Awesome/icons/">http://fortawesome.github.io/Font-Awesome/icons/</a> and enter the code','idoneita'),
			'section'	=> 'socialmedia',
			'setting'	=> 'pwt_social_media_code2'
	));		
	
	$wp_customize->add_setting('pwt_social_media_link2',array(
			'default'	=> '',
			'sanitize_callback'	=> 'esc_url_raw'
	));
	
	$wp_customize->add_control('pwt_social_media_link2',array(
			'label'	=> __('Social Media Link 2','idoneita'),
			'section'	=> 'socialmedia',
			'setting'	=> 'pwt_social_media_link2'
	));		

	$wp_customize->add_setting('pwt_social_media_code3',array(
			'default'	=> '',
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_social_media_code3',array(
			'label'	=> __('Social Media Code 3','idoneita'),
			'description' => __('Select a icon in this list <a target="_blank" href="http://fortawesome.github.io/Font-Awesome/icons/">http://fortawesome.github.io/Font-Awesome/icons/</a> and enter the code','idoneita'),
			'section'	=> 'socialmedia',
			'setting'	=> 'pwt_social_media_code3'
	));		
	
	$wp_customize->add_setting('pwt_social_media_link3',array(
			'default'	=> '',
			'sanitize_callback'	=> 'esc_url_raw'
	));
	
	$wp_customize->add_control('pwt_social_media_link3',array(
			'label'	=> __('Social Media Link 3','idoneita'),
			'section'	=> 'socialmedia',
			'setting'	=> 'pwt_social_media_link3'
	));		

	$wp_customize->add_setting('pwt_social_media_code4',array(
			'default'	=> '',
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_social_media_code4',array(
			'label'	=> __('Social Media Code 4','idoneita'),
			'description' => __('Select a icon in this list <a target="_blank" href="http://fortawesome.github.io/Font-Awesome/icons/">http://fortawesome.github.io/Font-Awesome/icons/</a> and enter the code','idoneita'),
			'section'	=> 'socialmedia',
			'setting'	=> 'pwt_social_media_code4'
	));		
	
	$wp_customize->add_setting('pwt_social_media_link4',array(
			'default'	=> '',
			'sanitize_callback'	=> 'esc_url_raw'
	));
	
	$wp_customize->add_control('pwt_social_media_link4',array(
			'label'	=> __('Social Media Link 4','idoneita'),
			'section'	=> 'socialmedia',
			'setting'	=> 'pwt_social_media_link4'
	));		

	$wp_customize->add_setting('pwt_social_media_code5',array(
			'default'	=> '',
			'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('pwt_social_media_code5',array(
			'label'	=> __('Social Media Code 5','idoneita'),
			'description' => __('Select a icon in this list <a target="_blank" href="http://fortawesome.github.io/Font-Awesome/icons/">http://fortawesome.github.io/Font-Awesome/icons/</a> and enter the code','idoneita'),
			'section'	=> 'socialmedia',
			'setting'	=> 'pwt_social_media_code5'
	));		
	
	$wp_customize->add_setting('pwt_social_media_link5',array(
			'default'	=> '',
			'sanitize_callback'	=> 'esc_url_raw'
	));
	
	$wp_customize->add_control('pwt_social_media_link5',array(
			'label'	=> __('Social Media Link 5','idoneita'),
			'section'	=> 'socialmedia',
			'setting'	=> 'pwt_social_media_link5'
	));			
	
}
add_action( 'customize_register', 'idoneita_customize_register' );

function idoneita_sanitize_integer( $input ) {
    if( is_numeric( $input ) ) {  return intval( $input ); }
}
