<?php
/**
 * The template for displaying all pages.
 *
 * @package Idoneita
 */
get_header(); ?>
 <?php while (have_posts()) : the_post(); ?> 
			<section class="section section-page-title">
				<div class="container">
					<div class="gutter">
						<h4><?php the_title(); ?></h4>
					</div>
				</div> <!--  END container  -->
			</section> <!--  END section-page-title  -->
			<div class="section section-blog">
				<div class="container">
					<div class="column-container blog-container">
						<div class="column-9-12 left">
							<div class="gutter">
								<div class="inner-page-container">
									<article class="single-post clearfix">
										<div class="article-text">
											<?php the_content(); ?>
										</div>
										<p><?php posts_nav_link(); ?></p>
										<div class="padinate-page"><?php wp_link_pages(); ?></div> 											
										<div class="comments">
											<?php comments_template(); ?>
										</div>										
									</article>
								</div>
							</div>
						</div>
						<div class="column-3-12 right">
							<div class="gutter">
								<?php  get_sidebar(); ?>
							</div>
						</div>						
					</div>
				</div> <!--  ENd container  -->
			</div> <!--  END section-blog  -->
<?php endwhile; ?>		
<?php get_footer(); ?>