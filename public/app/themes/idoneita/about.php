<?php
/**
 * Idoneita About Theme
 *
 * @package Idoneita
 */
?>

<?php

// About theme info

add_action( 'admin_menu', 'idoneita_abouttheme' );

function idoneita_abouttheme() {   
 	
	add_theme_page( __('About Theme', 'idoneita'), __('About Theme', 'idoneita'), 'edit_theme_options', 'about-idoneita', 'idoneita_guide');   
	
} 

if( ! function_exists( 'admin_idoneita_enqueue_styles' ) ) {
	function admin_idoneita_enqueue_styles() {

		wp_enqueue_style( 'idoneita-style-admin', get_template_directory_uri() . '/assets/css/style-admin.css', array(), '1.0' );		
	}
	add_action( 'admin_enqueue_scripts', 'admin_idoneita_enqueue_styles' );
}

//guidline for about theme

function idoneita_guide() { 
?>
<div class="wrapper-info">
	<div class="aedifcator-box" style="margin: 0 100px 0 0;">
   		<div style="border-bottom: 1px solid #ccc; font-size: 21px; font-weight: bold; padding: 40px 0 20px;">
			<?php esc_attr_e('About Idoneita Theme', 'idoneita'); ?>
		</div>
		<p><?php esc_attr_e('Idoneita is an awesome theme with fully responsive and compatible with newest version of WordPress, is easy to customizable, SEO Optimizable, Fast loading and an awesome panel options. Idoneita Theme is perfect for a construction business, but also for various other business or personal blog, The customization of this theme is very easy.','idoneita'); ?></p>
		<div class="proversion">
			<h3><?php esc_attr_e('Upgrade to Pro version!', 'idoneita'); ?></h3>
				<a class="upgradepro" target="_blank" href="<?php echo esc_url('https://www.pwtthemes.com/theme/idoneita-responsive-wordpress-theme'); ?>" target="_blank"><?php esc_attr_e('UPGRADE TO PRO', 'idoneita'); ?></a>
				<a class="livepreviw" target="_blank" href="<?php echo esc_url('https://www.pwtthemes.com/demo/idoneita'); ?>" target="_blank"><?php esc_attr_e('LIVE PREVIEW', 'idoneita'); ?></a>
			<p><?php esc_attr_e('If you need assistance, please do not hesitate to', 'idoneita'); ?> <a target="_blank" href="<?php echo esc_url('http://www.pwtthemes.com/contact'); ?>"><?php esc_attr_e('contact us', 'idoneita'); ?></a></p>
	        <h3><?php esc_attr_e('FREE vs PRO', 'idoneita'); ?></h3>

    	</div>		   
		<a href="<?php echo esc_url('https://www.pwtthemes.com/theme/idoneita-responsive-wordpress-theme'); ?>" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/images/demo/freevspro.jpg" alt="" /></a>
	</div>
</div>
<?php } ?>