<?php
/**
 * The template for displaying archive
 *
 * @package Idoneita
 */
get_header();
?>
<section class="section section-page-title">
	<div class="container">
		<div class="gutter">
			<h4><?php the_archive_title(); ?></h4>
		</div>
	</div> <!--  END container  -->
</section> <!--  END section-page-title  -->
<div class="section section-blog">
	<div class="container">
		<div class="column-container blog-container">		
			<div class="column-9-12 left">
				<div class="gutter">
					<div class="inner-page-container">
						<?php while (have_posts()) : the_post(); ?>
							<?php get_template_part( 'content', 'posts' ); ?>
						<?php endwhile; ?>						
						<p class="pagination">
						<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } else { ?>
								<span class="left button-gray"><?php next_posts_link(__('Previous Posts', 'idoneita')) ?></span>
								<span class="right button-gray"><?php previous_posts_link(__('Next posts', 'idoneita')) ?></span>			
						<?php } ?>
						</p>
					</div>
				</div>
			</div>
			<div class="column-3-12 right">
				<div class="gutter">
				    <?php  get_sidebar(); ?>
				</div>
			</div>			
		</div>
	</div> <!--  ENd container  -->
</div> <!--  END section-blog  -->
<?php get_footer(); ?>