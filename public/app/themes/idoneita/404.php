<?php
/**
 * The template for displaying page NOT FOUND.
 *
 * @package Idoneita
 */
get_header(); ?>
<section class="section section-page-title">
	<div class="container">
		<div class="gutter">
			<h4><?php _e( 'Not found', 'idoneita' ); ?></h4>
		</div>
	</div> <!--  END container  -->
</section> <!--  END section-page-title  -->
<div class="section section-blog">
	<div class="container">
		<div class="column-container blog-container">	
			<div class="column-12-12 left">
				<div class="gutter">
					<div class="inner-page-container">
						<article class="single-post clearfix">
							<div class="article-text">
								<p><?php _e( 'Sorry, but you are looking for something that isn\'t here.', 'idoneita' ); ?></p>
							</div>
						</article>
					</div>
				</div>
			</div>
		</div>
	</div> <!--  ENd container  -->
</div> <!--  END section-blog  -->
<?php get_footer(); ?>