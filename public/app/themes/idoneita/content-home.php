<?php 
/**
 * 
 * @package Idoneita 
 */
?>
<?php if( get_theme_mod('pwt_slider_content1') or get_theme_mod('pwt_slider_content2')) { ?>
<div class="owl-carousel previewtheme-carousel">
	<?php 
	if( get_theme_mod('pwt_slider_content1')) { 
	$queryslider = new WP_query('page_id='.get_theme_mod('pwt_slider_content1' ,true)); 
	while( $queryslider->have_posts() ) : $queryslider->the_post();
	?> 
	<div class="item" <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?> style="background-image: url('<?php echo esc_url(wp_get_attachment_url( get_post_thumbnail_id($post->ID))); ?>')"  <?php  endif; ?>>
		<div class="container clearfix">
			<div class="previewtheme column-6-12 right text-left animate-right-left">
				<div class="gutter">
					<h4><span><?php the_title(); ?></span></h4>
					<div class="icons-theme"><?php the_excerpt(); ?></div>
					<?php if( get_theme_mod('pwt_slider_button_text_1')) { ?>
					<div class="button-container">
						<a class="button-large" href="<?php the_permalink(); ?>"><?php echo esc_html(get_theme_mod('pwt_slider_button_text_1')); ?></a>
					</div>
					<?php } ?>	
				</div>
			</div>
		</div>
	</div>	
	<?php endwhile; wp_reset_postdata(); ?>
	<?php } ?>		
	<?php 
	if( get_theme_mod('pwt_slider_content2')) { 
	$queryslider = new WP_query('page_id='.get_theme_mod('pwt_slider_content2' ,true)); 
	while( $queryslider->have_posts() ) : $queryslider->the_post();
	?> 
	<div class="item" <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?> style="background-image: url('<?php echo esc_url(wp_get_attachment_url( get_post_thumbnail_id($post->ID))); ?>')"  <?php  endif; ?>>
		<div class="container clearfix">
			<div class="previewtheme column-6-12 right text-left animate-right-left">
				<div class="gutter">
					<h4><span><?php the_title(); ?></span></h4>
					<div class="icons-theme"><?php the_excerpt(); ?></div>
					<?php if( get_theme_mod('pwt_slider_button_text_2')) { ?>
					<div class="button-container">
						<a class="button-large" href="<?php the_permalink(); ?>"><?php echo esc_html(get_theme_mod('pwt_slider_button_text_2')); ?></a>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>	
	<?php endwhile; wp_reset_postdata(); ?>
	<?php } ?>		
</div>
<?php }  ?>
<section class="section section-themedescr">
	<div class="section-title">
		<div class="container">
			<div class="gutter">
				<h4><?php echo esc_html(get_theme_mod('pwt_info_box_title')); ?></h4>
			</div>
		</div>
	</div> <!--  END section-title  -->
	<div class="container">
		<div class="column-container themedescr-container">

			<div class="column-3-12">
				<div class="gutter">
					<article class="article-themedescr">
						<div class="article-icon">
							<a class="fa fa-<?php echo sanitize_html_class(get_theme_mod('pwt_whyus_icon_1',__( 'bookmark', 'idoneita' ))); ?>" href="<?php echo esc_url(get_theme_mod('pwt_whyus_link_1',__( '#', 'idoneita' ))); ?>"></a>
						</div>
						<h2><a href="<?php echo esc_url(get_theme_mod('pwt_whyus_link_1',__( '#', 'idoneita' ))); ?>"><?php echo esc_html(get_theme_mod('pwt_whyus_title_1',__( 'Why Us', 'idoneita' ))); ?></a></h2>
						<p><?php echo esc_html(get_theme_mod('pwt_whyus_content_1')); ?></p>
						<a class="button" href="<?php echo esc_url(get_theme_mod('pwt_whyus_link_1',__( '#', 'idoneita' ))); ?>"><?php echo esc_html(get_theme_mod('pwt_whyus_button_link_1',__( 'Read More', 'idoneita' ))); ?></a>
					</article>
				</div>
			</div>	
			<div class="column-3-12">
				<div class="gutter">
					<article class="article-themedescr">
						<div class="article-icon">
							<a class="fa fa-<?php echo sanitize_html_class(get_theme_mod('pwt_whyus_icon_2',__( 'laptop', 'idoneita' ))); ?>" href="<?php echo esc_url(get_theme_mod('pwt_whyus_link_2',__( '#', 'idoneita' ))); ?>"></a>
						</div>
						<h2><a href="<?php echo esc_url(get_theme_mod('pwt_whyus_link_2',__( '#', 'idoneita' ))); ?>"><?php echo esc_html(get_theme_mod('pwt_whyus_title_2',__( 'Our Vision', 'idoneita' ))); ?></a></h2>
						<p><?php echo esc_html(get_theme_mod('pwt_whyus_content_2')); ?></p>
						<a class="button" href="<?php echo esc_url(get_theme_mod('pwt_whyus_link_2',__( '#', 'idoneita' ))); ?>"><?php echo esc_html(get_theme_mod('pwt_whyus_button_link_2',__( 'Read More', 'idoneita' ))); ?></a>
					</article>
				</div>
			</div>	
			<div class="column-3-12">
				<div class="gutter">
					<article class="article-themedescr">
						<div class="article-icon">
							<a class="fa fa-<?php echo sanitize_html_class(get_theme_mod('pwt_whyus_icon_3',__( 'graduation-cap', 'idoneita' ))); ?>" href="<?php echo esc_url(get_theme_mod('pwt_whyus_link_3',__( '#', 'idoneita' ))); ?>"></a>
						</div>
						<h2><a href="<?php echo esc_url(get_theme_mod('pwt_whyus_link_3',__( '#', 'idoneita' ))); ?>"><?php echo esc_html(get_theme_mod('pwt_whyus_title_3',__( 'Our Location', 'idoneita' ))); ?></a></h2>
						<p><?php echo esc_html(get_theme_mod('pwt_whyus_content_3')); ?></p>
						<a class="button" href="<?php echo esc_url(get_theme_mod('pwt_whyus_link_3',__( '#', 'idoneita' ))); ?>"><?php echo esc_html(get_theme_mod('pwt_whyus_button_link_3',__( 'Read More', 'idoneita' ))); ?></a>
					</article>
				</div>
			</div>	
			<div class="column-3-12">
				<div class="gutter">
					<article class="article-themedescr">
						<div class="article-icon">
							<a class="fa fa-<?php echo sanitize_html_class(get_theme_mod('pwt_whyus_icon_4',__( 'support', 'idoneita' ))); ?>" href="<?php echo esc_url(get_theme_mod('pwt_whyus_link_4',__( '#', 'idoneita' ))); ?>"></a>
						</div>
						<h2><a href="<?php echo esc_url(get_theme_mod('pwt_whyus_link_4',__( '#', 'idoneita' ))); ?>"><?php echo esc_html(get_theme_mod('pwt_whyus_title_4',__( 'Support', 'idoneita' ))); ?></a></h2>
						<p><?php echo esc_html(get_theme_mod('pwt_whyus_content_4')); ?></p>
						<a class="button" href="<?php echo esc_url(get_theme_mod('pwt_whyus_link_4',__( '#', 'idoneita' ))); ?>"><?php echo esc_html(get_theme_mod('pwt_whyus_button_link_4',__( 'Read More', 'idoneita' ))); ?></a>
					</article>
				</div>
			</div>				

		</div>
	</div> <!--  END container  -->
</section> <!--  END section-themedescr  -->
<?php while (have_posts()) : the_post(); ?> 
<div class="section section-blog">
	<div class="container">
		<div class="column-container">
			<div class="column-12-12">
				<div class="gutter">
					<div class="inner-page-container">
						<article class="single-post clearfix">
							<div class="article-text">
								<?php the_content(); ?>
							</div>
						</article>
					</div>
				</div>
			</div>						
		</div>
	</div> <!--  ENd container  -->
</div> <!--  END section-blog  -->
<?php endwhile; ?>	
<section class="section section-recent-news">
	<div class="section-title">
		<div class="container">
			<div class="gutter">
				<h4><?php echo esc_html(get_theme_mod('pwt_blog_page_title',__( 'Latest Blog', 'idoneita' ))); ?></h4>
			</div>
		</div>
	</div> <!--  END section-title  -->
	<div class="container">
		<div class="column-container news-container">
			<?php 
			$i=0;
			$idoneita_get_list_posts = idoneita_get_list_posts(3);
			while ( $idoneita_get_list_posts->have_posts() ) {
			$idoneita_get_list_posts->the_post(); $i++;
			if($i<=3) {
			?>	
			<div class="column-4-12">
				<div class="gutter">
					<article class="article-news">
					    <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="article-image">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('idoneita-photo-360-240'); ?></a>
							<a class="fa fa-plus" href="<?php the_permalink(); ?>"></a>
						</div>
						<?php endif; ?>
						<div class="article-text">
							<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
							<p><?php the_excerpt(); ?></p>
							<p class="meta">
							<span class="column-12-12 left"><?php the_time(get_option( 'date_format')); ?></span>
							</p>
						</div>
					</article>
				</div>
			</div>						
			<?php }} ?>		
		</div>
	</div> <!--  END container  -->
</section> <!--  END section-recent-news  -->