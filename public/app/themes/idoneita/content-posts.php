<?php 
/**
 * 
 * @package Idoneita 
 */
?>
<article class="article-blog clearfix">
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="article-info">
			<div class="posted-rounded"><span class="large"><?php the_time('d'); ?></span> <span><?php the_time('M'); ?></span></div>
			<p><?php _e( 'By', 'idoneita' ); ?> <span><?php the_author(); ?></span></p>
		</div>
		<div class="article-details">
			<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
			<div class="article-image">
				<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('idoneita-photo-800-500'); ?></a>
			</div>
			<?php endif; ?>
			<div class="article-text">
				<h2><a href="<?php the_permalink() ?>"><?php if(get_the_title($post->ID)) { the_title(); } else { the_time( get_option( 'date_format' ) ); } ?></a></h2>
				<p><?php the_excerpt(); ?></p>
				<div class="meta-table">
					<div class="table-cell left"><p><?php _e( 'Comments', 'idoneita' ); ?>: <?php  comments_popup_link( __( 'Post a Comment', 'idoneita' ), __( '1 Comment', 'idoneita' ), __( '% Comments', 'idoneita' ), 'comments-link' , __( 'Comments are Closed', 'idoneita' )); ?></p></div>
					<div class="table-cell center"><p><?php _e( 'Categories', 'idoneita' ); ?>: <?php the_category(', '); ?></p></div>
					<div class="table-cell right"><p><a class="learn-more" href="<?php the_permalink() ?>"><?php _e( 'Read More', 'idoneita' ); ?></a></p></div>
				</div>
			</div>
		</div>
	</div>
</article>