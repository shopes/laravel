<?php
/**
 *
 * @package Idoneita
 */
 get_header(); ?>
 <?php while (have_posts()) : the_post(); ?>
		<section class="section section-page-title">
			<div class="container">
				<div class="gutter">
					<h4><?php echo esc_html(get_theme_mod('pwt_blog_page_title',__( 'Latest Blog', 'idoneita' ))); ?></h4>
				</div>
			</div> <!--  END container  -->
		</section> <!--  END section-page-title  -->
		<div class="section section-blog">
			<div class="container">
				<div class="column-container blog-container">	
					<div class="column-9-12 left">
						<div class="gutter">
							<div class="inner-page-container">
								<article class="single-post clearfix">
									<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
									<div class="article-image">
										<?php the_post_thumbnail('idoneita-photo-800-500'); ?>
									</div>						
									<?php endif; ?>	
									<div class="article-text">
										<div class="meta-table">
											<div class="table-cell left"><p><?php _e( 'Comments', 'idoneita' ); ?>: <?php  comments_popup_link( __( 'Post a Comment', 'idoneita' ), __( '1 Comment', 'idoneita' ), __( '% Comments', 'idoneita' ), 'comments-link' , __( 'Comments are Closed', 'idoneita' )); ?></p></div>
											<div class="table-cell center"><p><?php _e( 'Categories', 'idoneita' ); ?>: <?php the_category(', '); ?></p></div>
											<div class="table-cell right"><p><?php _e( 'By', 'idoneita' ); ?>: <?php the_author(); ?></p></div>
										</div>	
                                       
										<h2><?php the_title(); ?></h2>
										<?php the_content(); ?>
										<p class="tags"><span></span> <?php the_tags(); ?></p>
									</div>
									<p><?php posts_nav_link(); ?></p>
									<div class="padinate-page"><?php wp_link_pages(); ?></div> 	
									<div class="comments">
										<?php comments_template(); ?>
									</div>										
								</article>
							</div>
						</div>
					</div>
					<div class="column-3-12 right">
						<div class="gutter">
							<?php  get_sidebar(); ?>
						</div>
					</div>						
				</div>
			</div> <!--  ENd container  -->
		</div> <!--  END section-blog  -->
<?php endwhile; ?>
<?php get_footer(); ?>