<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Tortuga
 */

global $current_user;
wp_get_current_user()

?>
        <!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href={{ asset("static/AdminLTE/bootstrap/css/bootstrap.min.css") }}>
    <link rel="stylesheet" href={{ asset("static/AdminLTE/dist/css/AdminLTE.min.css") }}>
    <link rel="stylesheet" href={{ asset("static/AdminLTE/dist/css/skins/skin-red-light.css") }}>
    <link rel="stylesheet" href="https://cdn.staticfile.org/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.staticfile.org/ionicons/2.0.1/css/ionicons.min.css">
<?php wp_head(); ?>
<!-- Font Awesome -->
    <!-- Ionicons -->
    <!-- Theme style -->
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <script>
        if (top != self) {
            top.location.href = self.location.href;
            document.write('<style>html{display: none;}</style>');
        }
    </script>
</head>

<body <?php body_class(); ?> >


    <div id="page" class="hfeed site skin-red-light">

        <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'tortuga'); ?></a>

        <div id="header-top" class="header-bar-wrap"><?php do_action('tortuga_header_bar'); ?></div>

        <header id="masthead" class="site-header clearfix" role="banner">

            <div class="header-main container clearfix">

                <div id="logo" class="site-branding clearfix">

                    <?php tortuga_site_logo(); ?>
                    <?php tortuga_site_title(); ?>
                    <?php tortuga_site_description(); ?>

                </div><!-- .site-branding -->

                <div class="header-widgets clearfix">

                    <?php // Display Header Widgets.
                    if (is_active_sidebar('header')) :

                        dynamic_sidebar('header');

                    endif; ?>
                </div><!-- .header-widgets -->

            </div><!-- .header-main -->

            <div id="main-navigation-wrap" class="primary-navigation-wrap">

                <nav id="main-navigation" class="primary-navigation navigation container clearfix" role="navigation">
                    <?php
                    // Display Main Navigation.
                    wp_nav_menu(array(
                            'theme_location' => 'primary',
                            'container' => false,
                            'menu_class' => 'main-navigation-menu',
                            'echo' => true,
                            'fallback_cb' => 'tortuga_default_menu',
                        )
                    );
                    ?>
                </nav><!-- #main-navigation -->

            </div>

        </header><!-- #masthead -->

        <?php tortuga_breadcrumbs(); ?>

        <?php tortuga_header_image(); ?>

        <div id="content" class="{{ View::shared('content_class',"site-content container") }} clearfix">
