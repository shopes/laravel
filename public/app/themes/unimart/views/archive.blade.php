@extends('wp::layout.default')

<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Tortuga
 */

//get_header();

//kd(Kernel::$view->renderSections());

// Get Theme Options from Database.
$theme_options = tortuga_theme_options();
?>

@section('page-header')
    <?php the_archive_title('<h1 class="archive-title">', '</h1>'); ?>
    <?php the_archive_description('<div class="archive-description">', '</div>'); ?>
@stop

@section('page-header-wrap')
    <header class="page-header">
        @yield('page-header')
    </header><!-- .page-header -->
@stop

@section('content')
    <section id="primary" class="content-archive content-area">
        <main id="main" class="site-main" role="main">

            <?php
            if ( have_posts() ) : ?>

            @yield('page-header-wrap')

            <div id="post-wrapper" class="post-wrapper clearfix">

                <?php while (have_posts()) : the_post();

                    get_template_part('template-parts/content');

                endwhile; ?>

            </div>

            <?php tortuga_pagination(); ?>

            <?php
            else :

                get_template_part('template-parts/content', 'none');

            endif; ?>

        </main><!-- #main -->
    </section><!-- #primary -->

    @if('three-columns' !== $theme_options['post_layout'])

        <?php get_sidebar(); ?>

    @endif

@stop

<?php //get_footer(); ?>
