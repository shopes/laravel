
@section("footer")

    <div id="footer" class="footer-wrap">

        <footer id="colophon" class="site-footer container clearfix" role="contentinfo">

            <div id="footer-text" class="site-info">
                <?php do_action('tortuga_footer_text'); ?>
            </div><!-- .site-info -->

            <?php do_action('tortuga_footer_menu'); ?>

        </footer><!-- #colophon -->

    </div>
    @stop


    </div><!-- #content -->

    <?php do_action('tortuga_before_footer'); ?>


    </div><!-- #page -->
    <script src={{asset("static/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js")}}></script>

    <?php wp_footer(); ?>

    @yield("footer")
    <!-- jQuery 2.2.3 -->
    <!-- Bootstrap 3.3.6 -->
    <script src={{asset("static/AdminLTE/bootstrap/js/bootstrap.min.js")}}></script>
    <script src={{asset("static/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js")}}></script>
    <!-- AdminLTE App -->
    <script src={{asset("static/AdminLTE/dist/js/app.js")}}></script>



    </body>
    </html>
