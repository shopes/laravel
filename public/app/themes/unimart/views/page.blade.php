<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Tortuga
 */

//get_header();
?>

@extends("wp::layout.admin")

@section("sidebar-menu")
    <?php
    wp_nav_menu(
        array(
            'theme_location' => 'admin_left_nav',
            "menu" => "admin_left_nav",
            //'depth' => 2,
            //'container' => 'div',
            //'container_class' => 'collapse navbar-collapse',
            //'container_id' => 'bs-example-navbar-collapse-1',
            'menu_class' => 'sidebar-menu',
            'fallback_cb' => 'wp_adminlte_navwalker::fallback',
            'walker' => new wp_adminlte_navwalker(),
        )
    );
    ?>
@stop

@section("sidebar-menu")
    <ul class="sidebar-menu">
        <li class="header">HEADER</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Link</span></a></li>
        <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
        <li class="treeview">
            <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span>
                <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="#">Link in level 2</a></li>
                <li><a href="#">Link in level 2</a></li>
            </ul>
        </li>
    </ul>
@stop

@section("content-main")

    <section id="primary" class="content-single content-area">
        <main id="main" class="site-main" role="main">

            <?php while(have_posts()) : the_post();

                //$bladeTplName = get_field('bladeTplName');

                get_template_part('template-parts/content', 'page');

                comments_template();

            endwhile; ?>

        </main><!-- #main -->
    </section><!-- #primary -->

    <?php get_sidebar(); ?>

@stop

@section("content-footer")
    @parent
    {{--    <script>
           jQuery(function ($) {
              $("section.content").load("http://blog.shopes.cn/home/wp-admin/options-discussion.php #wpbody");
           });
        </script>--}}
@stop

@section("content")
    @if(function_exists('get_field') && $adminPage = get_field('admin_page'))
        <iframe id="frame_admin"
                name="frame_admin"
                marginheight="0"
                marginwidth="0"
                style="margin-bottom: -8px;"
                width="100%"
                height="780"
                onload="jQuery('#frame_admin').height(jQuery('#frame_admin').contents().find('#wpcontent').height())"
                frameborder="0"
                src="{{ admin_url($adminPage) }}"
                scrolling="no"
        >

        </iframe>

        <script type="text/javascript">
            //setInterval(function(){
            jQuery(function ($) {
                setInterval(function () {
                    try {
                        var content = jQuery('#frame_admin').contents();
//                    var find = content.find('#wpwrap');
//                    if(find.length > 0){
//                        jQuery('#frame_admin').height(find.height());
//                    } else {
                        jQuery('#frame_admin').height(content.height());
//                    }
                    } catch (e) {
                    }

                }, 150);

            });

            //},100);
        </script>
    @else
        @parent
    @endif
@stop
