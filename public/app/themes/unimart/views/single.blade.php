<?php
/**
 * The template for displaying all single posts.
 *
 * @package Tortuga
 */

//get_header();

?>
@extends("wp::layout.admin")

@section("content-main")
    <section id="primary" class="content-single content-area">
        <main id="main" class="site-main" role="main">

            <?php while(have_posts()) : the_post();

                get_template_part('template-parts/content', 'single');

                tortuga_related_posts();

                comments_template();

            endwhile; ?>

        </main><!-- #main -->
    </section><!-- #primary -->

    <?php get_sidebar(); ?>
@stop

<?php //get_footer(); ?>
