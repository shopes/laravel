@section('sidebar')
    <?php
    /**
     * The sidebar containing the main widget area.
     *
     * @package Tortuga
     */

    // Check if Sidebar has widgets.
    if ( is_active_sidebar('sidebar') ) : ?>

    <section id="secondary" class="sidebar widget-area clearfix" role="complementary">

        <?php dynamic_sidebar('sidebar'); ?>

    </section><!-- #secondary -->

    <?php
    endif;
    ?>

@stop

@section('header')
    <?php
    /**
     * The header for our theme.
     *
     * Displays all of the <head> section and everything up till <div id="content">
     *
     * @package Tortuga
     */

    ?><!DOCTYPE html>
    <html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>

        <div id="page" class="hfeed site">

            <a class="skip-link screen-reader-text"
               href="#content"><?php esc_html_e('Skip to content', 'tortuga'); ?></a>

            <div id="header-top" class="header-bar-wrap"><?php do_action('tortuga_header_bar'); ?></div>

            <header id="masthead" class="site-header clearfix" role="banner">

                <div class="header-main container clearfix">

                    <div id="logo" class="site-branding clearfix">

                        <?php tortuga_site_logo(); ?>
                        <?php tortuga_site_title(); ?>
                        <?php tortuga_site_description(); ?>

                    </div><!-- .site-branding -->

                    <div class="header-widgets clearfix">

                        <?php // Display Header Widgets.
                        if (is_active_sidebar('header')) :

                        dynamic_sidebar('header');

                        endif; ?>

                    </div><!-- .header-widgets -->

                </div><!-- .header-main -->

                <div id="main-navigation-wrap" class="primary-navigation-wrap">

                    <nav id="main-navigation" class="primary-navigation navigation container clearfix"
                         role="navigation">
                        <?php
                        // Display Main Navigation.
                        wp_nav_menu(array(
                        'theme_location' => 'primary',
                        'container' => false,
                        'menu_class' => 'main-navigation-menu',
                        'echo' => true,
                        'fallback_cb' => 'tortuga_default_menu',
                        )
                        );
                        ?>
                    </nav><!-- #main-navigation -->

                </div>

            </header><!-- #masthead -->

            <?php tortuga_breadcrumbs(); ?>

            <?php tortuga_header_image(); ?>

            <div id="content" class="site-content container clearfix">
            @show

            @section('content')
                <!-- main content area -->
                @show

                @section('footer')
                    <?php
                    /**
                     * The template for displaying the footer.
                     *
                     * Contains all content after the main content area and sidebar
                     *
                     * @package Tortuga
                     */

                    ?>

            </div><!-- #content -->

            <?php do_action( 'tortuga_before_footer' ); ?>

            <div id="footer" class="footer-wrap">

                <footer id="colophon" class="site-footer container clearfix" role="contentinfo">

                    <div id="footer-text" class="site-info">
                        <?php do_action( 'tortuga_footer_text' ); ?>
                    </div><!-- .site-info -->

                    <?php do_action( 'tortuga_footer_menu' ); ?>

                </footer><!-- #colophon -->

            </div>

        </div><!-- #page -->

        <?php wp_footer(); ?>

    </body>
    </html>

@show