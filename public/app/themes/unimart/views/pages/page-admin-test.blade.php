@extends("wp::page")

@section("config")
    @parent
    <?php

    /**
     * The template used for displaying page content in page.php
     *
     * @package Tortuga
     */
    View::share('sidebar', false);
    View::share('comments', false);
    View::share('content_area_class', "container")

    ?>
@stop

@section("page_content")

    <article id="post-<?php the_ID(); ?>" <?php //post_class(); ?>>

        <?php the_post_thumbnail(); ?>

        <div class="hide">
            <header class="entry-header">

                <?php the_title('<h1 class="page-title">', '</h1>'); ?>

            </header><!-- .entry-header -->
        </div>


        <div class="entry-content clearfix">

            <?php the_content(); ?>

            <?php wp_link_pages(array(
            'before' => '<div class="page-links">' . esc_html__('Pages:', 'tortuga'),
            'after' => '</div>',
            )); ?>

        </div><!-- .entry-content -->

    </article>

@stop
