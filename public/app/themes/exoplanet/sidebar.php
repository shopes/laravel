<?php
/**
 * The sidebar containing the main widget area
 *
 * @package Exoplanet
 */

$exoplanet_sidebar_layout = get_post_meta( $post->ID, 'exoplanet_sidebar_layout', true );
if(!$exoplanet_sidebar_layout){
	$exoplanet_sidebar_layout = 'right_sidebar';
}

if ( $exoplanet_sidebar_layout == "no_sidebar" || $exoplanet_sidebar_layout == "no_sidebar_condensed" ) {
	return;
}

if( is_active_sidebar('exoplanet-right-sidebar') &&  $exoplanet_sidebar_layout == "right_sidebar" ){
	?>
	<div id="secondary" class="widget-area">
		<?php dynamic_sidebar('exoplanet-right-sidebar'); ?>
	</div><!-- #secondary -->
	<?php
}

if( is_active_sidebar('exoplanet-left-sidebar') &&  $exoplanet_sidebar_layout == "left_sidebar" ){
	?>
	<div id="secondary" class="widget-area">
		<?php dynamic_sidebar('exoplanet-left-sidebar'); ?>
	</div><!-- #secondary -->
	<?php
}