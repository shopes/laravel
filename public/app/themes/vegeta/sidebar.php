<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package howlthemes
 */

if(!is_active_sidebar('sidebar-1')) {
    //return;
}
?>

<div id="secondary" class="widget-area" role="complementary" itemscope="itemscope"
     itemtype="http://schema.org/WPSideBar">
    
    <?php vegeta_socialmediafollow(); ?>
    <div id="phone-wrap" style="position: relative;">
        <img src="assets/img/phone320x568.png"/>
        <!--/static/iscroll.html-->
        <iframe id="phone" name="phone" frameborder="0" marginwidth="0" marginheight="0" allowtransparency="1"
                src="http://localhost:8080/home"
                allowfullscreen="allowfullscreen"></iframe>
    </div>
    <?php dynamic_sidebar('sidebar-1'); ?>
</div><!-- #secondary -->
<style type="text/css">
    #phone {
        background: #fff;
        position: absolute;
        top: 49px;
        left: 11px;
        width:320px;
        height: 568px;
    }
</style>