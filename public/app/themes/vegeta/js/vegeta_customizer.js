/**
 * Theme Customizer related js
 */

jQuery(document).ready(function() {

   jQuery('#customize-info .accordion-section-title').append('<a target="_blank" style="text-transform: uppercase; background: #D54E21; color: #fff; font-size: 10px; line-height: 14px; padding: 2px 5px; display: inline-block;" href="http://www.howlthemes.com/vegetapro-blogging-wordpress-theme/" target="_blank">{pro}</a>'.replace('{pro}',vegeta_customizer_obj.pro));
});