<?php
/**
 * WordPress基础配置文件。
 *
 * 这个文件被安装程序用于自动生成wp-config.php配置文件，
 * 您可以不使用网站，您需要手动复制这个文件，
 * 并重命名为“wp-config.php”，然后填入相关信息。
 *
 * 本文件包含以下配置选项：
 *
 * * MySQL设置
 * * 密钥
 * * 数据库表名前缀
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/zh-cn:%E7%BC%96%E8%BE%91_wp-config.php
 *
 * @package WordPress
 */

define('WP_CACHE', true);

if(!defined("LARAVEL_ROOT")) {
    require_once __DIR__ . '/../../bootstrap/autoload.php';
    require_once LARAVEL_ROOT . '/bootstrap/app.php';
}


if(!defined('ABSPATH')) {
    return false;
}

(new Dotenv\Dotenv(LARAVEL_ROOT))->load();

define('DB_FILE',"default.sqlite.php");
define('DB_DIR',base_path('database/sqlite'));
define('USE_MYSQL',true);


$site = $_SERVER['SERVER_NAME'];
$sitepart = explode('.', $site);
$s1 = array_pop($sitepart);
$s2 = array_pop($sitepart);

define('COOKIE_DOMAIN', "{$s2}.{$s1}");
define('ADMIN_COOKIE_PATH', '/');
define('COOKIEPATH', '/');
define('SITECOOKIEPATH', '/');
define('COOKIEHASH', substr(md5(__DIR__), 0, 4));

//define('NOBLOGREDIRECT', env('APP_URL'));
define('WP_POST_REVISIONS', 3);
define('WP_DEFAULT_THEME', 'twentyfifteen');


if(PHP_SAPI != 'cli' && env('WP_MULTISITE', false)) {
    define('WP_ALLOW_MULTISITE', true);
    define('MULTISITE', true);
    define('SUBDOMAIN_INSTALL', true);
    define('DOMAIN_CURRENT_SITE', "$s2.$s1");
    define('PATH_CURRENT_SITE', '/');
    define('SITE_ID_CURRENT_SITE', 1);
    define('BLOG_ID_CURRENT_SITE', 1);
} elseif(1) {
    define('WP_ALLOW_MULTISITE', false);
    define('MULTISITE', false);

}

define('RELOCATE', true);

define('WP_HOME', env('APP_URL', '/blog'));
define('WP_SITEURL', WP_HOME);//. '/home'
define('WP_CONTENT_DIR', __DIR__);
define('WP_CONTENT_URL', WP_HOME . '/app');//dirname($_SERVER['SCRIPT_NAME']).

// ** MySQL 设置 - 具体信息来自您正在使用的主机 ** //
/** WordPress数据库的名称 */
define('DB_NAME', env('DB_DATABASE', 'blog'));

/** MySQL数据库用户名 */
define('DB_USER', env('DB_USERNAME', 'root'));

/** MySQL数据库密码 */
define('DB_PASSWORD', env('DB_PASSWORD', 'root'));

/** MySQL主机 */
define('DB_HOST', env('DB_HOST', 'localhost') . ':' . env('DB_PORT', '3306'));

/** 创建数据表时默认的文字编码 */
define('DB_CHARSET', 'utf8mb4');

/** 数据库整理类型。如不确定请勿更改 */
define('DB_COLLATE', '');

/**#@+
 * 身份认证密钥与盐。
 *
 * 修改为任意独一无二的字串！
 * 或者直接访问{@link https://api.wordpress.org/secret-key/1.1/salt/
 * WordPress.org密钥生成服务}
 * 任何修改都会导致所有cookies失效，所有用户将必须重新登录。
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'iRXtr7x*a=)s|jrKAr;GZ@7,.bS$W&JA7-Dz4MAH!O8jAcL>rq$N$eJ,{8DO=,KX');
define('SECURE_AUTH_KEY', '~U6O9vCvjoBV5N6&cG/AkKH/=y6wXL/pg@nZcq9g]T}ge.Nq@Qnzhhim/>bjk)X ');
define('LOGGED_IN_KEY', 'ZGJkx$Rf)c{rSU7CbgB7c2ozQ#[5hC~})D.AqJNY`4;}Bx)14X|ybZ_`y3YndtUZ');
define('NONCE_KEY', '3>E9h}?GS;4ehXYx8y`Ws+pe+m%G]N E&F{d3<q[|G/lN,;h$Qej4[}w2DCIbU9Z');
define('AUTH_SALT', '{;.h+C*:O _i.q=DBmc|/[rDV8TcQ@9w i}8mX&;~L.t)%b4Il:5[uC2A/xvKY48');
define('SECURE_AUTH_SALT', '1{m9Qq|+hGG3)QfjBl2:}kK<7JgV[*ute++UkxeOFOB.&tp3p51;`u2X*zm~ue;w');
define('LOGGED_IN_SALT', 'MT_xxVE;-o`tiYBq4(frGy;zUI;wY>oy7lRj;`:p3=qtuA!h: F-/;tr9Wi~u|+W');
define('NONCE_SALT', '7AqRH +-Auh2uLl}!>T X~? a}9czy=)Rh?|9+#7!4w?yJ3cYP)zL Enzy/DWv]c');

/**#@-*/

/**
 * WordPress数据表前缀。
 *
 * 如果您有在同一数据库内安装多个WordPress的需求，请为每个WordPress设置
 * 不同的数据表前缀。前缀名只能为数字、字母加下划线。
 */
$table_prefix = env('DB_TABLEPRE','cloudshop_'). env('WP_PREFIX', 'topic_');

/**
 * 开发者专用：WordPress调试模式。
 *
 * 将这个值改为true，WordPress将显示所有用于开发的提示。
 * 强烈建议插件开发者在开发环境中启用WP_DEBUG。
 *
 * 要获取其他能用于调试的信息，请访问Codex。
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/**
 * zh_CN本地化设置：启用ICP备案号显示
 *
 * 可在设置→常规中修改。
 * 如需禁用，请移除或注释掉本行。
 */
define('WP_ZH_CN_ICP_NUM', true);

/* 好了！请不要再继续编辑。请保存本文件。使用愉快！ */

/** WordPress目录的绝对路径。 */
//if(!defined('ABSPATH')) {
//    define('ABSPATH', dirname(__FILE__) . '/');
//}

/** 设置WordPress变量和包含文件。 */
//require_once(ABSPATH . 'wp-settings.php');
require_once(__DIR__ . '/core/wp-settings.php');
