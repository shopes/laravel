<?php
/**
 * Created by IntelliJ IDEA.
 * User: Mo
 * Date: 2016/12/9
 * Time: 下午5:37
 */

//require_once LARAVEL_ROOT.'/bootstrap/autoload.php';
//require_once LARAVEL_ROOT.'/bootstrap/app.php';

use App\Kernel;
use Hashids\Hashids;

add_action('admin_init', [Kernel::class, 'run']);
add_action('template_redirect', [Kernel::class, 'run']);
add_action( 'before_signup_header' , [Kernel::class, 'run']);




//add_action('init', function () {
add_action('wp', function () {
    if (is_404()) {
        status_header(200);
    }
});

/**
 * WordPress新文章自动使用ID作为别名
 * 雅兮网整理：http://www.iyaxi.com
 */
add_action('save_post', 'using_id_as_slug', 10, 2);

function using_id_as_slug($post_id, $post)
{
    global $post_type;
    if($post_type == 'post') { //只对文章生效
        // 如果是文章的版本，不生效
        if(wp_is_post_revision($post_id)) {
            return false;
        }
        // 取消挂载该函数，防止无限循环
        remove_action('save_post', 'using_id_as_slug');
        // 使用文章ID作为文章的别名
        $hashids = new Hashids('post', 6, '23456789abcdefghijkmnpqrstuvwxyz'); // pad to length 10
        wp_update_post(array('ID' => $post_id, 'post_name' => $hashids->encode($post_id)));
        // 重新挂载该函数
        add_action('save_post', 'using_id_as_slug');
    }
}

/**
 * 修改WordPress旧文章别名为文章ID
 * 雅兮网整理：http://www.iyaxi.com
 */

if($_GET['post_name_id'] == 'yes') {
    add_action('init', function() {
        query_posts('posts_per_page=-1');
        while(have_posts()) {
            the_post();
            $post_id = $GLOBALS['post']->ID;
            $hashids = new Hashids('post', 6, '23456789abcdefghijkmnpqrstuvwxyz'); // pad to length 10
            $postName = $hashids->encode($post_id);
            if($GLOBALS['post']->post_name != $postName) {
                wp_update_post(array(
                    'ID' => $post_id,
                    'post_name' => $postName
                ));
            }
            
        }
        wp_reset_query();
    });
}

add_action("admin_bar_menu", function(WP_Admin_Bar $wp_admin_bar) {
    if(is_admin()) {
        return;
    }
    //kd($wp_admin_bar->get_nodes());
});

//function disable_bar_updates() {
//    global $wp_admin_bar;
//    $wp_admin_bar->remove_menu('updates');
//}
//add_action( 'wp_before_admin_bar_render', 'disable_bar_updates' );


add_action('admin_bar_menu', function(WP_Admin_Bar $wp_admin_bar) {
    if(!is_admin_bar_showing() || is_admin() ) {
        return;
    }
    $wp_admin_bar->remove_menu('site-name');
    $wp_admin_bar->add_node(array(
            'id' => 'site-name',
            'title' => is_user_logged_in() ? "后台管理" : '',
            // get_bloginfo('name'),
            'href' => (is_admin() || !current_user_can('read')) ? home_url('/') : admin_url('options-general.php'),
            // get_bloginfo('home_url'),
            //'meta' => array('target' => '_blank')
        )
    );
}, 31);

add_action('admin_bar_menu', function(WP_Admin_Bar $wp_admin_bar) {
    $wp_admin_bar->remove_menu('updates');
    $wp_admin_bar->remove_menu('comments');
    if(is_admin() || is_user_logged_in()){
        return;
    }
    $wp_admin_bar->add_node( array(
        'id' => 'loginform',
        'parent' => 'top-secondary',
        'title' => '<form action="' .wp_login_url(). '" method="post" class="form-inline" id="adminbar-loginform">
                <input type="text" id="inputUname" name="log" placeholder="Username" class="admin-input" />
                <input type="password" id="inputPassword" name="pwd" placeholder="Password" class="admin-input" />
                <label class="checkbox hide">
                <input type="checkbox" name="rememberme" id="rememberme" checked="checked" value="forever" class="admin-check" /> Remember me</label>
                <input type="hidden" name="redirect_to" value="' .$_SERVER['REQUEST_URI']. '" />
                <button type="submit" class="btn admin-button" name="submit" value="Send">登录</button>
            </form>'
    ) );
}, 100);

add_action("after_setup_theme",function(){
    show_admin_bar( true );
});

add_filter("logout_url", function($logouturl, $redir){
    return $logouturl . '&amp;redirect_to=' . urlencode($_SERVER['REQUEST_URI']);
    
},10,2);

